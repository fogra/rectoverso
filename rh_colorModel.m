%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

classdef rh_colorModel < handle
	
	properties (Constant)
		%geo		% Constants used for easy mode-discrimination
	
	end
	
	properties %(Hidden)
		%geo		% Constants used for easy mode discrimination
		
		parm	% Externally set parameters
		
		illu_type = 'diffuse'; % <-- macht wohl so keinen Sinn (?)
		meas_type = '0�'; % <-- macht wohl so keinen Sinn (?)
		
		reflData% reflectance measurement data READ from a CGATS file
		tranData% transmittance measurement data READ from a CGATS file
		mediumData% Measured Spectrum of white backing
		outData % Output of Prediction, to be WRITTEN to CGATS file
		
		lambda	% start/end/delta/nsamp
		paper	% R/T/rho/tau 
		Hebert	% the parameters of the Hebert model
		
		%					1	2	3	4	5	 6	  7		8	 9	 10	  11   12	13	  14	15	  16
		%cm.colorantsList={'W','C','M','Y','CM','CY','MY','CMY','K','CK','MK','YK','CMK','CYK','MYK','CMYK'};
		colorantsList % = {'W','C','M','Y','CM','CY','MY','CMY',..};
		%colorantsRefl	% struct array
		%colorantsTran	% struct array
		colorants		% struct array. Contains dev, Refl, Tran of the Neugebauer colorants
        wedgeRefl		% contains the step wedges of all the Neugebauer primaries (from the chart)
        wedgeTran		% contains the step wedges of all the Neugebauer primaries (from the chart)
		mediumMeas		% contains the medium measurements
		
		dbg		% Data for Debugging, e.g. for copying into Graphics
	end
	% The following properties can be set only by class methods
	properties (SetAccess = private) %GetAccess = private
	end
	
	events
		%InsufficientFunds 
	end
	
	methods %#############################################################
		% Constructor ********************************************
		function cm = rh_colorModel()
			
% 			cm.geo.diff = 1; % Constants used for easy mode-discrimination
% 			cm.geo.dir45= 2;
% 			cm.geo.dir0 = 3;

			
			% Now specify the lambda params that shall be used throughout
			% the program. If read data do not match these, they must be
			% adjusted accordingly.
			cm.lambda.start = 380;
			%cm.lambda.end = 700; % 780;	############# TEST wenn < 730 #################
			cm.lambda.end = 780;	% Init. Can be changed in parameter file.
			cm.lambda.delta = 10;
			cm.lambda.nsamp = (cm.lambda.end - cm.lambda.start) / cm.lambda.delta + 1;
			
			% DIE 2^N COLORANTS. DIE REIHENFOLGE DARF NICHT GE�NDERT WERDEN, OHNE
			% DEN ZUGRIFF AUF DIE DEMICHEL-GLEICHUNGEN AUCH ZU �NDERN!!!!!
			cm.colorantsList = {'W','C','M','Y','CM','CY','MY','CMY','K','CK','MK','YK','CMK','CYK','MYK','CMYK'};
			
		end
	  
		% Destructor ******************************************** 
		function delete(obj)
			%if obj.paperCol ~= 0
			%	delete(obj.paperCol);
			%end
			disp('rh_colorModel gel�scht');
		end
		
		
		%********************************************************
		%function res = load_measData(cm, refl_file, tran_file, mediumWhite_file)
		function res = load_measData(cm)
			if (cm.parm.WAVELENGTH_RANGE_UPPER_LIMIT >= 700 && cm.parm.WAVELENGTH_RANGE_UPPER_LIMIT <=780)
				cm.lambda.end = cm.parm.WAVELENGTH_RANGE_UPPER_LIMIT;
				% If the upper lambda limit is < 700, then the number of parameter
				% slots for c/b in Hebert_paper_params() will be too small
				cm.lambda.nsamp = (cm.lambda.end - cm.lambda.start) / cm.lambda.delta + 1;
            end
            
			% Load measurement data from an ISO/CGATS file, whose locations have been
			% specified within the parameter file
			dataF	= [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER];%'data/';
            
            devFile = rh_CGATS([dataF,cm.parm.DEVICE_VALUE_FILE_NAME]);
            devFile.read_CGATS_file(cm.lambda,true);     
			
			%## Reflection data...
			refl_file	= cm.parm.REFLECTANCE_MEASUREMENTS_FILE_NAME;
			
			cm.reflData = rh_CGATS([dataF,refl_file]);
			cm.reflData.read_CGATS_file(cm.lambda,true);
			if strfind(cm.reflData.Mode,'tran')
				resp = input('### This does not seem to be reflection measurements (as it is supposed to be)! Are you sure you want to proceed?');
            end
            
            if isempty(cm.reflData.dev) % CMYK nicht gefunden
				cm.reflData.dev = devFile.dev;
				%clear('devFile'); do not delete, will need it later...
			end				
			res = cm.reflData.strip_duplicates('dev');
			res = cm.reflData.write_IT874_visual();
			
			
			%## Transmission data...
			tran_file	= cm.parm.TRANSMISSION_MEASUREMENTS_FILE_NAME;
			
			cm.tranData = rh_CGATS([dataF,tran_file]);
			cm.tranData.read_CGATS_file(cm.lambda,true);
			if strfind(cm.reflData.Mode,'refl')
				resp = input('### This does not seem to be transmission measurements (as it is supposed to be)! Are you sure you want to proceed?');
			end
			if isempty(cm.tranData.dev) % CMYK nicht gefunden
				%devFile = rh_CGATS([dataF,cm.parm.DEVICE_VALUE_FILE_NAME]);
				%devFile.read_CGATS_file(cm.lambda,true);
				cm.tranData.dev = devFile.dev;
				clear('devFile');
			end				
			res = cm.tranData.strip_duplicates('dev');
			res = cm.tranData.write_IT874_visual();
			
			%## Medium data...
			if ~strcmp(cm.parm.MEDIUM_MEASUREMENTS_FILE_NAME,'')
				mediumWhite_file	= cm.parm.MEDIUM_MEASUREMENTS_FILE_NAME;
			
				cm.mediumData = rh_CGATS([dataF,mediumWhite_file]);
				cm.mediumData.read_CGATS_file(cm.lambda,true);
				
			else % 4 Medium Measurement Files...
				cm.mediumData = rh_CGATS([dataF,cm.parm.MEDIUM_MEASUREMENTS_NAMEBASE,'Medium_Trans',cm.parm.MEDIUM_MEASUREMENTS_NAMEEXT]);
				cm.mediumData.read_CGATS_file(cm.lambda,true);
				cm.mediumData.ensure_samplenames({'TransR','TransV'},'tran');
				
				CGtmp = rh_CGATS([dataF,cm.parm.MEDIUM_MEASUREMENTS_NAMEBASE,'Medium_Refl0',cm.parm.MEDIUM_MEASUREMENTS_NAMEEXT]);
				CGtmp.read_CGATS_file(cm.lambda,true);
				CGtmp.ensure_samplenames({'ReflR','ReflV'},'refl');
				if CGtmp.Aperture > 2
					resp = input(['\n### Aperture for reflection measurements on the diffuser plate should	###\n'...
						'### only be 2mm, as the diffuser plate is only 6mm in size!	###\n'...
						'### You can proceed, but the results will be compromised!	###\n'...
						'### Press return to proceed...		###']);
				end
				cm.mediumData.append_samples(CGtmp);
				
				CGtmp.filename = [dataF,cm.parm.MEDIUM_MEASUREMENTS_NAMEBASE,'Medium_ReflBlack',cm.parm.MEDIUM_MEASUREMENTS_NAMEEXT];
				CGtmp.read_CGATS_file(cm.lambda,true);
				CGtmp.ensure_samplenames({'ReflR_Bb', 'ReflV_Bb','ReflBackBlack'},'refl');
				cm.mediumData.append_samples(CGtmp);
				
				CGtmp.filename = [dataF,cm.parm.MEDIUM_MEASUREMENTS_NAMEBASE,'Medium_ReflWhite',cm.parm.MEDIUM_MEASUREMENTS_NAMEEXT];
				CGtmp.read_CGATS_file(cm.lambda,true);
				CGtmp.ensure_samplenames({'ReflR_Bw', 'ReflV_Bw', 'ReflBackWhite'},'refl');
				cm.mediumData.append_samples(CGtmp);
				
				clear('CGtmp');
			end
			res = cm.mediumData.strip_duplicates('sampName');
			
			
			res = 0;
		end
			
				
		%********************************************************
		function res = find_colorants(cm,mode)
			% Finds the 2^N Neugebauer colorants in the respective data set and copies them into
			% the dedicated arrays, to be easily found.
			
            	
			nsamp = cm.lambda.nsamp;
			len  = length(cm.colorantsList);
			for coli=1:len
				%if coli < iStart || coli > iEnd,	continue;	end
				
				%searchCol = 'MK';
				searchCol = char(cm.colorantsList(coli) );
				
				searchDev = [0,0,0,0];
				for i=1:length(searchCol)
					pattern = searchCol(i);
					idx=strfind('CMYK',pattern);
					if idx >= 1
						searchDev(idx) = 100;
					end
				end
				
				if strcmp(mode, 'refl')
					cm.colorants.Refl(coli,1:nsamp) = cm.find_spec_by_devdata(mode,searchDev);
                    if searchCol ~= 'W'
                        tmp = cm.find_wedge(mode,searchCol);
                        cm.wedgeRefl{coli} = tmp;
                    end
				elseif strcmp(mode, 'tran')
					cm.colorants.Tran(coli,1:nsamp) = cm.find_spec_by_devdata(mode,searchDev);
                    if searchCol ~= 'W'
                        tmp = cm.find_wedge(mode,searchCol);
                        cm.wedgeTran{coli} = tmp;
                    end
				end
				cm.colorants.dev(coli,:) = searchDev;
				
				
				
			end
		end
		
		
		%********************************************************
		function outdev = dev_with_inklimits(cm,dev)
			outdev = dev;
			collist = {'CMYK','CMY','CMK','CYK','MYK','CM','CY','MY','CK','MK','YK','C','M','Y','K'};
			for coli=1:length(collist)
				colidx = cm.get_colorant_idx(collist(coli)); % der index in der Neugebauer-Liste
				if cm.colorants.inklimits(colidx) < 100
					searchCol = char(collist(coli) );
					% welcher canal wird limitiert?
					chlist = [];
					for i=1:length(searchCol)
						pattern = searchCol(i);
						idx=strfind('CMYK',pattern);
						if idx >= 1,	chlist(end+1) = idx;	end
					end
					
					%if length(chlist) < 4
						sumval = sum(outdev(chlist));
						sumlimit = cm.colorants.inklimits(colidx) * length(chlist);
						if sumval > sumlimit
							for ii=1:length(chlist)
								outdev(chlist(ii)) = outdev(chlist(ii)) * sumlimit/sumval;
								%outdev(chlist(2)) = outdev(chlist(2)) * sumlimit/sumval;
							end
						end

					%elseif length(chlist) == 1
					%	if outdev(chlist(1)) > cm.colorants.inklimits(colidx)
					%		outdev(chlist(1)) = cm.colorants.inklimits(colidx);
					%	end
					%else
					%	brichmicih
					%end
				end
			end

		end
		
		
		%********************************************************
		function res = modify_colorants(cm)
			% 
			
            	
			nsamp = cm.lambda.nsamp;
			len  = length(cm.colorantsList);
			for coli=2:len
				modified = false;
				devneu = cm.dev_with_inklimits(cm.colorants.dev(coli,:));
				if sum(devneu ~= cm.colorants.dev(coli,:) )
					modified = true;
					cm.colorants.dev(coli,:) = devneu;
					
					spec = cm.find_spec_by_devdata('refl',devneu);
					if spec == -1
						% existierende Farbe suchen, die die Bedingungen erf�llt...
						wlen = length(cm.wedgeRefl{1,coli}.dev);
						found = false;
						for ii=1:wlen
							if cm.wedgeRefl{1,coli}.dev(ii,1) > devneu(1),	continue; end
							if cm.wedgeRefl{1,coli}.dev(ii,2) > devneu(2),	continue; end
							if cm.wedgeRefl{1,coli}.dev(ii,3) > devneu(3),	continue; end
							if cm.wedgeRefl{1,coli}.dev(ii,4) > devneu(4),	continue; end
							found = true; % gefunden
							break;
						end
						if ~found,	bloedsinn; end
						devneu = cm.wedgeRefl{1,coli}.dev(ii,:);
						cm.colorants.dev(coli,:) = devneu;
						cm.colorants.Refl(coli,:) = cm.find_spec_by_devdata('refl',devneu);
						cm.colorants.Tran(coli,:) = cm.find_spec_by_devdata('tran',devneu);
					else
						cm.colorants.Refl(coli,:) = spec;
						cm.colorants.Tran(coli,:) = cm.find_spec_by_devdata('tran',devneu);
					end
				
					% Nun die TVI-Kurven anpassen, so dass bei der neuen Farbe auch
					% das Max. herauskommt
					newlim = max(devneu)/100;
					for ii=1:length(cm.wedgeRefl{coli}.TVcurve)
						if cm.wedgeRefl{coli}.TVcurve(ii,1) > newlim
							cm.wedgeRefl{coli}.TVcurve(ii,2) = 1;
						elseif abs(cm.wedgeRefl{coli}.TVcurve(ii,1) - newlim) < eps
							factor = 1/cm.wedgeRefl{coli}.TVcurve(ii,2);
							cm.wedgeRefl{coli}.TVcurve(ii,2) = 1;
						else
							cm.wedgeRefl{coli}.TVcurve(ii,2) = cm.wedgeRefl{coli}.TVcurve(ii,2) * factor;
						end
						cm.wedgeRefl{coli}.TVcurve(ii,3) = 0; % Derivative
					end
				end
				%{
				if cm.colorants.inklimits(coli) == 100,		continue;	end
				searchCol = char(cm.colorantsList(coli) ); %searchCol = 'MK';
				searchDev = cm.colorants.dev(coli,:) * cm.colorants.inklimits(coli)/100;
				cm.colorants.dev(coli,:) = searchDev;
                   %} 
			end
		end
		
		
		%********************************************************
		function y = analyse_data(cm)
			
			cm.find_colorants('refl'); 
			cm.find_colorants('tran'); 
			
			cm.calc_TVI('refl');
			cm.calc_TVI('tran');

			p1=cm.plot_TVI(2,4,101,'thick');
			p2=cm.plot_TVI(5,8,102,'thick');
			p3=cm.plot_TVI(9,12,103,'thick');
			p4=cm.plot_TVI(13,16,104,'thick');
			
			cm.find_ink_limits();

			if cm.parm.DATA_ELIMINATE_SATURATED_COLORS
				%now modify Neug. colorants
				cm.modify_colorants();
				
				cm.plot_TVI(2,4,p1,'thin');
				cm.plot_TVI(5,8,p2,'thin');
				cm.plot_TVI(9,12,p3,'thin');
				cm.plot_TVI(13,16,p4,'thin');
			end
		end
		
		
		%********************************************************
		function y = find_ink_limits(cm)
			
			for ii=2:16
				if isempty(cm.wedgeRefl{ii}.dev) || length(cm.wedgeRefl{ii}.dev(:,1)) < 3,		continue; end
				xx = cm.wedgeRefl{ii}.TVcurve(:,1);
				rr = cm.wedgeRefl{ii}.TVcurve(:,2);
				tt = cm.wedgeTran{ii}.TVcurve(:,2);
				% Calculate derivatives...
				for jj=1:length(xx)-1 % The curves start at the top
					%if jj<length(xx)
						deriv = (rr(jj) - rr(jj+1)) / (xx(jj) - xx(jj+1));
						cm.wedgeRefl{ii}.TVcurve(jj,3) = deriv;
						deriv = (tt(jj) - tt(jj+1)) / (xx(jj) - xx(jj+1));
						cm.wedgeTran{ii}.TVcurve(jj,3) = deriv;
					%end
					if jj>1
						%deriv2 = (cm.wedgeRefl{ii}.TVcurve(jj-1,3) - cm.wedgeRefl{ii}.TVcurve(jj,3) ) / (xx(jj-1) - xx(jj));
						deriv2 = (cm.wedgeRefl{ii}.TVcurve(jj-1,3) - cm.wedgeRefl{ii}.TVcurve(jj,3) ); % Steigungs�nderung. Kein Nenner, da x-Werte ungegelm��ig
						cm.wedgeRefl{ii}.TVcurve(jj,4) = deriv2;
						deriv2 = (cm.wedgeTran{ii}.TVcurve(jj-1,3) - cm.wedgeTran{ii}.TVcurve(jj,3) ); % Steigungs�nderung. Kein Nenner, da x-Werte ungegelm��ig
						cm.wedgeTran{ii}.TVcurve(jj,4) = deriv2;
					end
				end
				% Now determine limits...
				startDerivRefl = cm.wedgeRefl{ii}.TVcurve(end-1,3);
				%for jj=1:length(xx)-1
				%	if cm.wedgeRefl{ii}.TVcurve(jj,3) > 0.2 * startDerivRefl 
				%		break; % limit found
				%	end
				%end
				%limitRefl = cm.wedgeRefl{ii}.TVcurve(jj,1) * 100;
				
				% minimum und max. betrag der 2. Ableit. suchen
				% Kriterion: if the min. curvature (2nd derivative) = max(abs(2nd deriv.)), there is a limit
				mini2 = 1000;	jmin = -1;
				maxabs2 = -1000;
				for jj=1:length(xx)-1
					if mini2 > cm.wedgeRefl{ii}.TVcurve(jj,4) 
						mini2 = cm.wedgeRefl{ii}.TVcurve(jj,4);
						jmin = jj;
					end
					if maxabs2 < abs(cm.wedgeRefl{ii}.TVcurve(jj,4) )
						maxabs2 = abs(cm.wedgeRefl{ii}.TVcurve(jj,4) );
					end
				end
				%{
				if mini2 == - maxabs2 % strongest edge
				%if mini2 < - maxabs2/2 % strongest edge
					%...cm.wedgeRefl{ii}.TVcurve(jmin-1,3) < 0.3 * startDerivRefl %Steigung oberhalb auch klein genug?
					done=false;
					while ~done
						if cm.wedgeRefl{ii}.TVcurve(jmin-1,3) < 0.2 %Steigung oberhalb auch klein genug?
							limitRefl = round(cm.wedgeRefl{ii}.TVcurve(jmin,1) * 100);
							done=true;
						elseif cm.wedgeRefl{ii}.TVcurve(jmin-1,4) < 0 % Kr�mmung des n�chsten auch < 0?
							jmin=jmin-1;
						else
							limitRefl = 100;
							done=true;
						end
					end
				else % kein Limit
				%}
					% Die Steigung von unten heraus durchgehen.
					limitRefl=-1;
					for jj=length(xx)-1:-1:2
						if cm.wedgeRefl{ii}.TVcurve(jj,1) < 0.5,	continue;	end
						if cm.wedgeRefl{ii}.TVcurve(jj-1,3) < 0.15		% found Plateau 
							limitRefl = round(cm.wedgeRefl{ii}.TVcurve(jj,1) * 100);
							break;
						end
					end
					if limitRefl < 0
						limitRefl = 100;
					end
				%end
				
				
				
				startDerivTran = cm.wedgeTran{ii}.TVcurve(end-1,3);
% 				for jj=1:length(xx)-1 
% 					if cm.wedgeTran{ii}.TVcurve(jj,3) > 0.2 * startDerivTran
% 						break; % limit found
% 					end
% 				end
%				limitTran = cm.wedgeTran{ii}.TVcurve(jj,1) * 100;

				mini2 = 1000;	jmin = -1;
				maxabs2 = -1000;
				for jj=1:length(xx)-1
					if mini2 > cm.wedgeTran{ii}.TVcurve(jj,4) 
						mini2 = cm.wedgeTran{ii}.TVcurve(jj,4);
						jmin = jj;
					end
					if maxabs2 < abs(cm.wedgeTran{ii}.TVcurve(jj,4) )
						maxabs2 = abs(cm.wedgeTran{ii}.TVcurve(jj,4) );
					end
				end
				%{
				if mini2 == - maxabs2 % strongest edge
				%if mini2 < - maxabs2/2 % strongest edge
					done=false;
					while ~done
						if cm.wedgeTran{ii}.TVcurve(jmin-1,3) < 0.2 %Steigung oberhalb auch klein genug?
							limitTran = round(cm.wedgeTran{ii}.TVcurve(jmin,1) * 100);
							done=true;
						elseif cm.wedgeTran{ii}.TVcurve(jmin-1,4) < 0 % Kr�mmung des n�chsten auch < 0?
							jmin=jmin-1;
						else
							limitTran = 100;
							done=true;
						end
					end
				else % kein Limit
				%}
					% Die Steigung von unten heraus durchgehen.
					limitTran=-1;
					for jj=length(xx)-1:-1:2
						if cm.wedgeTran{ii}.TVcurve(jj,1) < 0.5,	continue;	end
						if cm.wedgeTran{ii}.TVcurve(jj-1,3) < 0.15		% found Plateau 
							limitTran = round(cm.wedgeTran{ii}.TVcurve(jj,1) * 100);
							break;
						end
					end
					if limitTran < 0
						limitTran = 100;
					end
				%end
				
				cm.colorants.inklimits(ii) = min(limitRefl,limitTran);
				if cm.colorants.inklimits(ii) < 50
					cm.colorants.inklimits(ii) = 50; % Soo stark d�rfen wir nicht begrenzen, sonst sind schon die Prim�rkan�le begrenzt.
				end
				if cm.colorants.inklimits(ii) < 100
					disp(['	Found ink saturation @ ',char(cm.colorantsList(ii)),'=',num2str(cm.colorants.inklimits(ii))]);
				end
			end
		end
		
		%********************************************************
		% Calculates the tone value curves for all the 16 Neugabauer primaries. The
		% TV curves are added to the 15 wedge sets determined earlier.
		% TVI = tone value increase = the increase of measured tone values as
		% compared to the device tone values
		% The TV curves are required for the Demichel equations, which in turn are
		% used to interpolate the scatter parameters.
		function res = calc_TVI(cm,mode)
			res=0;
			if strcmp(mode,'refl')
				wedgeIn = cm.wedgeRefl;
				whiteIn = cm.colorants.Refl(1,:);
			elseif strcmp(mode,'tran')
				wedgeIn = cm.wedgeTran;
				whiteIn = cm.colorants.Tran(1,:);
			else
				brichmich();
			end
			
			len = length(wedgeIn) ;
			lami = length(whiteIn);
			
			for nwedge=1:len
				if cm.colorantsList{nwedge}=='W', continue; end
				%wedge = [];
				
				if isempty(wedgeIn{nwedge}.dev) 
					resp = input(['##### WARNING! WEDGE ',num2str(nwedge),' IS EMPTY! Press RETURN to continue! #####']);
					continue
				elseif max(wedgeIn{nwedge}.dev(1,:)) ~= 100 
					resp = input(['##### WARNING! WEDGE ',num2str(nwedge),' SEEMS TO BE WRONG! Press RETURN to continue! #####']);
				end
				
				% gr��te Dynamik suchen... Annahme: Neugebauer-Vollton ist
				% an 1. Stelle
				maxDyn = -1;
				lamiDyn= -1;
				for ll=1:lami
					dyn= whiteIn(ll) - wedgeIn{nwedge}.spec(1,ll);
					if dyn > maxDyn
						if wedgeIn{nwedge}.spec(1,ll) > 1e-5 % need a minimum value, otherwise we get Inf as density
							maxDyn = dyn;
							lamiDyn= ll;
						end
					end
				end
				val = whiteIn(lamiDyn);
				Dwhite = log10(1/val);
				
				numcol = length(wedgeIn{nwedge}.spec(:,1));
				TVcurve = zeros(numcol+1,2);
				
				val = wedgeIn{nwedge}.spec(1,lamiDyn);
				den100 =log10(1/val);
				for coli=1:numcol
					%wedge.dev = wedgeIn{coli}.dev;
					val = wedgeIn{nwedge}.spec(coli,lamiDyn);
					den = log10(1/val);
					TV = (den-Dwhite)/(den100-Dwhite);
					if TV>1, TV=1; end
					
					tmp = wedgeIn{nwedge}.dev(coli,:);
					TVcurve(coli,1) = max(tmp)/100;
					TVcurve(coli,2) = TV;
				end
				TVcurve(coli+1,1) = 0; % Paper
				TVcurve(coli+1,2) = 0;
				
				if strcmp(mode,'refl')
					cm.wedgeRefl{nwedge}.TVcurve = TVcurve;
				else
					cm.wedgeTran{nwedge}.TVcurve = TVcurve;
				end
			end
			
		end
		
		
		%********************************************************
		function y = TVrefl_f(cm,NeugChan,x)
			
			y = interp1(cm.wedgeRefl{NeugChan}.TVcurve(:,1),cm.wedgeRefl{NeugChan}.TVcurve(:,2), x);
			
		end
		
		
		%********************************************************
		function idx = get_colorant_idx(cm,col)
			idx=-1;
			for i=1:length(cm.colorantsList)
				if strcmp(col,cm.colorantsList(i))
					idx=i;
					break
				end
			end
		end
			
		%********************************************************
		% retrieves the spec and dev values of those colors, defined by the idx.
		% idx may be a single or a list of indices of the 16 Neugebauer colorants
		function spec = get_colorant_spec(cm,mode,idx)
			% Copies the spectrum of the desired colorant, given by its index in the
			% colorant list
			if strcmp(mode, 'refl')
				%spec = cm.colorantsRefl(idx,1:cm.lambda.nsamp);
				lcolorants = cm.colorants.Refl;
			elseif strcmp(mode, 'tran')
				%spec = cm.colorantsTran(idx,1:cm.lambda.nsamp);
				lcolorants = cm.colorants.Tran;
			else
				spec = -1;
				return;
			end
			%dev = cm.colorants.dev;
			
			for ii=1:length(idx)
				spec(ii,:) = lcolorants(idx(ii),:);
				
			end
		end
		
		
		%********************************************************
		% cmData must be a struct containing a) .dev and b) .numSets 
		% (e.g. reflData or tranData)
		function idx = find_idx_by_devdata(cm,cmData,searchDev)
			idx = -1;
			for ii=1:cmData.numSets
				if cmData.dev(ii,1) ~= searchDev(1), continue, end
				if cmData.dev(ii,2) ~= searchDev(2), continue, end
				if cmData.dev(ii,3) ~= searchDev(3), continue, end
				if cmData.dev(ii,4) ~= searchDev(4), continue, end
				idx = ii;
				break;
			end
			
		end

			
		%********************************************************
		function spec = find_spec_by_devdata(cm,mode,searchDev)

			if strcmp(mode, 'refl')
				cmData = cm.reflData;
			elseif strcmp(mode, 'tran')
				cmData = cm.tranData;
			else
				res = -1;
				return;
			end
			
% 			foundIdx = -1;
% 			for ii=1:cmData.numSets
% 				if cmData.dev(ii,1) ~= searchDev(1), continue, end
% 				if cmData.dev(ii,2) ~= searchDev(2), continue, end
% 				if cmData.dev(ii,3) ~= searchDev(3), continue, end
% 				if cmData.dev(ii,4) ~= searchDev(4), continue, end
% 				foundIdx = ii;
% 				break;
% 			end
			foundIdx = cm.find_idx_by_devdata(cmData,searchDev);
			if foundIdx < 0
				spec = -1; % Kein Spektrum gefunden!
				return;
			end
			spec = cmData.spec(foundIdx,1:cm.lambda.nsamp);
		end
		
		
		%********************************************************
		function spec = find_spec_by_sampleName(cm,dSet,sampleName)
			len = length(sampleName);
			for ii=1:dSet.numSets
				%if strcmp(dSet.sampName(ii),sampleName)
				if strncmp(dSet.sampName(ii),sampleName,len)
					spec = dSet.spec(ii,:);
					return;
				end
			end
			spec = -1; % not found
		end
		
			
		%********************************************************
		function wedge = find_wedge(cm,mode,searchCol)

			if strcmp(mode, 'refl')
				cmData = cm.reflData;
			elseif strcmp(mode, 'tran')
				cmData = cm.tranData;
			else
				res = -1;
				return;
			end
			
			%searchCol = 'MK';
			%searchCol = char(primColor );
			searchDev = [0,0,0,0];
			chIdxl = [];
			for i=1:length(searchCol)
				pattern = searchCol(i);
				idx=strfind('CMYK',pattern);
				if idx >= 1
					pos = length(chIdxl) + 1;
					chIdxl(pos) = idx;
					searchDev(idx) = 'x'; % wird als ASCII code 120 abgelegt
				end
			end
			
			wedge.dev = [];
			wedge.spec = [];
			numCols = 0;
			%foundIdx = -1;
			for ii=1:cmData.numSets
				if searchDev(1) ~= 'x' && cmData.dev(ii,1) ~= searchDev(1), continue, end
				if searchDev(2) ~= 'x' && cmData.dev(ii,2) ~= searchDev(2), continue, end
				if searchDev(3) ~= 'x' && cmData.dev(ii,3) ~= searchDev(3), continue, end
				if searchDev(4) ~= 'x' && cmData.dev(ii,4) ~= searchDev(4), continue, end
				nextSet = false;
				for jj=2:length(chIdxl)
					if cmData.dev(ii,chIdxl(jj)) ~= cmData.dev(ii,chIdxl(1))
						nextSet = true;
						break;
					end
				end
				if nextSet, continue, end
				
				if sum(cmData.dev(ii,:)) == 0, continue, end % Wei� auslassen
				%foundIdx = ii;
				numCols = numCols+1;
				wedge.dev(numCols,:) = cmData.dev(ii,:);
				wedge.spec(numCols,:) = cmData.spec(ii,:);
			end
			
			if isempty(wedge.dev),	return;		end % Vermutlich unvollst�ndiges Chart

			sortTmp = [wedge.dev, wedge.spec];
			sortTmp = sortrows(sortTmp,[-1 -2 -3 -4]); % R�ckw�rts nach spalten 1..4 sortieren
			
			% Jetzt noch Duplikate beseitigen (durch Mittelung)...
			numDup = 1;
			numCols2 = numCols;
			for ii=2:numCols
				if sortTmp(ii,1) ~= sortTmp(ii-1,1), continue, end
				if sortTmp(ii,2) ~= sortTmp(ii-1,2), continue, end
				if sortTmp(ii,3) ~= sortTmp(ii-1,3), continue, end
				if sortTmp(ii,4) ~= sortTmp(ii-1,4), continue, end
				numDup = numDup+1; % Gleichen gefunden.
				
				% ev. noch mehr gleiche?...
				if ii<numCols
					if (sortTmp(ii,1:4) == sortTmp(ii+1,1:4)), continue, end %weitersuchen
				end
				% Jetzt mitteln...
				i1 = ii-numDup+1;
				tmp = sum(sortTmp(i1:ii,:),1);
				tmp = tmp/numDup;
				sortTmp(i1,:) = tmp;
				for jj=1:(numDup-1), sortTmp(i1+jj,1:4) = Inf; end
				numCols2 = numCols2 - (numDup-1);% less unique colors
				
				numDup = 1;% zur�cksetzen
			end
			sortTmp = sortrows(sortTmp,[-1 -2 -3 -4]);% nochmal sortieren
			
			wedge.dev = sortTmp(1:numCols2,1:4);
			wedge.spec = sortTmp(1:numCols2,5:end);
			
			%if foundIdx < 0
			%	spec = -1; % Kein Spektrum gefunden!
			%	return;
			%end
			%spec = cmData.spec(foundIdx,1:cm.lambda.nsamp);
		end
		
		
		
		%********************************************************
		function dE = calculate_dEresults(cm)
			disp('*** Calculating Delta E...');
			ct = rh_color(cm.outData.lambda.start, cm.outData.lambda.end, cm.outData.lambda.delta); % color transform object
			
			nlam = cm.outData.lambda.nsamp;
			if strcmp(cm.parm.PARAMETERIZATION_WHICH_SAMPLES,'Neugebauer16')
				specRef = cm.colorants.Tran(:,1:nlam);
				devRef	= cm.colorants.dev;
			else
				specRef = cm.tranData.spec(:,1:nlam);
				devRef	= cm.tranData.dev;
			end
			
			XYZref = ct.spec2XYZ(specRef);
			XYZ2   = ct.spec2XYZ(cm.outData.spec(:,1:nlam));
			
			for ii=1:length(XYZref)
				Labref(ii,:) = ct.XYZ2Lab(XYZref(ii,:));
				Lab2(ii,:) = ct.XYZ2Lab(XYZ2(ii,:));
			end
			
			dElist = ct.DeltaE_00(Labref, Lab2, 1, 1, 1);
			
			if cm.parm.DATA_ELIMINATE_SATURATED_COLORS
				%devNew = [];
				%dEnew  = [];
				dEliTmp = [];
				nn = 0;
				for ii=1:length(devRef)
					dev = cm.dev_with_inklimits(devRef(ii,:));
					if min(dev == devRef(ii,:)) % alle gleich
						nn = nn+1;
						%devNew(nn,:) = dev;
						%dEnew(nn)  = dElist(ii);
						dEliTmp(nn,1) = dElist(ii);
						dEliTmp(nn,2) = ii;
					end
				end
				%devRef = devNew;
				%dElist = dEnew';
				%clear('devNew');
				%clear('dEnew');
				dElist = dEliTmp(:,1);
				dE.limitColors = nn;
			else
				dEliTmp = [dElist, (1:length(dElist))'];
				
			end
			sortTmp = sortrows(dEliTmp,[1 2]); % Sort for columns 1 then 2	
			
            dE.dE = dElist;
			dE.allColors = length(XYZref);
			dE.limitColors = length(dElist);
			
			disp(['	Used Colors: ',num2str(dE.limitColors),'/',num2str(dE.allColors),'. Eliminated ',num2str(dE.allColors-dE.limitColors),' saturated colors.']);
			
			dE.mean = mean(dElist);
			dE.medi = median(dElist);
			dE.max  = max(dElist);
			dE.std  = std(dElist);
            dE.quant95 = quantile(dElist, 0.95);
			
			% Now find out where specific data are...
			
			dE.best.idx		= sortTmp(1,2);
			%dE.best.dev		= devRef(sortTmp(1,2),:);
			dE.best.dev		= devRef(dE.best.idx,:);
			dE.best.spec	= cm.outData.spec(dE.best.idx,:);
			dE.best.specRef = specRef(dE.best.idx,:);
			
			%mid = idivide(int32(length(XYZref)), 2);
			mid = idivide(int32(length(dElist)), 2);
			dE.median.idx	= sortTmp(mid,2);
			dE.median.spec	= cm.outData.spec(dE.median.idx,:);
			dE.median.specRef = specRef(dE.median.idx,:);
			dE.median.dev		= devRef(dE.median.idx,:);
			
			dE.worst.idx		= sortTmp(end,2);
			dE.worst.spec		= cm.outData.spec(dE.worst.idx,:);
			dE.worst.specRef	= specRef(dE.worst.idx,:);
			dE.worst.dev		= devRef(dE.worst.idx,:);
			
			% KOntrolle:
			XYZref = ct.spec2XYZ(dE.worst.specRef);
			XYZ2   = ct.spec2XYZ(dE.worst.spec);
			Labref = ct.XYZ2Lab(XYZref);
			Lab2   = ct.XYZ2Lab(XYZ2);
			dEtest = ct.DeltaE_00(Labref, Lab2, 1, 1, 1);
			if abs(dEtest - dE.max) > 0.001
				dE_Auswertung_hat_niciht_gefunzt
			end
			
			clear('ct');
		end
		
		
		%********************************************************
		function save_state(cm)
			save zustand_Hebert_calibrate cm
		end
		
% 		%********************************************************
% 		function load_state(cm)
% 			cm = load 'zustand_Hebert_calibrate'
% 		end
		
		
		
	  
	end % methods

	
	methods (Static = true) %#############################################################
		
	end
	
end % classdef