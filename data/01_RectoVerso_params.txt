% Parameter file for Hebert model operations
% Note: The parameter name and value must be separated by a tab. No other tabs may be used!
% Comments may be added after a single further tab or % in each line.

COLOR_DATA_FOLDER						"data/"
MEDIUM_SUBFOLDER						ID01/
REFLECTANCE_MEASUREMENTS_FILE_NAME		ID01_refl.txt
TRANSMISSION_MEASUREMENTS_FILE_NAME		ID01_trans.txt
DEVICE_VALUE_FILE_NAME 					"../Reference IT874-1680.txt" 	% only required if meas. files do not contain dev. values

MEDIUM_MEASUREMENTS_FILE_NAME			ID01_MediumMess.txt

WAVELENGTH_RANGE_UPPER_LIMIT			780			% must be 700 <= x <= 780
DATA_ASSIMILATE_MEDIUM_MEASUREMENTS		1			% 0 = no; 1 = yes
DATA_ELIMINATE_SATURATED_COLORS			1			% 0 = use all colors; 1 = eliminated saturated colors

PARAMETERIZATION_REFRACTIVE_INDEX		1.5			% standard value is 1.5
PARAMETERIZATION_SCATTER_MODEL			12			%% 0=none; 1=linear; 2=quadratic
PARAMETERIZATION_INPUT_GEOMETRY_REFL	dir45		% diffuse/dir45
PARAMETERIZATION_INPUT_GEOMETRY_TRAN	diffuse		% MUST be diffuse
PARAMETERIZATION_INPUT_MODE				refl		% refl/tran
PARAMETERIZATION_WHICH_SAMPLES			Chart       % Chart/Neugebauer16/WedgeX, where X in {C,M,Y,K,CM,CY...}
PARAMETERIZATION_INKSMOOTHNESS			0.01			% [0..1]

PREDICTION_OUTPUT_FILE_NAME				ID01-trans-predict.txt
PREDICTION_OUTPUT_MODE					tran		% refl/tran
PREDICTION_OUTPUT_GEOMETRY				diffuse		% diffuse/dir45
PREDICTION_DELTAE_FILENAME				ID01-trans_dE.txt
