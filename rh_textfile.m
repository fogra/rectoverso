%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

classdef rh_textfile < handle
	% Simple and preliminary object for reading and writing text parameter files
	
	properties %#############################################################
		filename
		rawfields
		
		parms
		
	end
	
	methods %#############################################################
		% Constructor ********************************************
		function ob = rh_textfile(filename)
			ob.filename		= filename;
			
		end
		
		
		%********************************************************
		function res = read_text_file(ob, doDisplay)
			if doDisplay, disp(['Loading ',ob.filename,'...']); end
			%{
			fid = fopen(ob.filename);
			
			if fid < 0
				disp(['ERROR: File ',ob.filename,' could not be opened!']);
				res = -1;
				return;
			end
			
			maxNumFields = 2;
			tex='%s';
			for i=1:maxNumFields
				tex = [tex,' %s'];
			end
			mydata = textscan(fid, tex);
			fclose(fid);
			%}
			ob.read_textfile_fields(4,'\t%'); % reads the raw text fields to the object; max 2 columns
			
			
			
			[row,val] = ob.searchTag(ob.rawfields, 1, 'COLOR_DATA_FOLDER','string');
			ob.parms.COLOR_DATA_FOLDER = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'MEDIUM_SUBFOLDER','string');
			ob.parms.MEDIUM_SUBFOLDER = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'REFLECTANCE_MEASUREMENTS_FILE_NAME','string');
			ob.parms.REFLECTANCE_MEASUREMENTS_FILE_NAME = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'TRANSMISSION_MEASUREMENTS_FILE_NAME','string');
			ob.parms.TRANSMISSION_MEASUREMENTS_FILE_NAME = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'DEVICE_VALUE_FILE_NAME','string');
			ob.parms.DEVICE_VALUE_FILE_NAME = val;
			
			[row,val] = ob.searchTag(ob.rawfields, 1, 'MEDIUM_MEASUREMENTS_FILE_NAME','string');
			ob.parms.MEDIUM_MEASUREMENTS_FILE_NAME = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'MEDIUM_MEASUREMENTS_NAMEBASE','string');
			ob.parms.MEDIUM_MEASUREMENTS_NAMEBASE = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'MEDIUM_MEASUREMENTS_NAMEEXT','string');
			ob.parms.MEDIUM_MEASUREMENTS_NAMEEXT = val;
			
			
			[row,val] = ob.searchTag(ob.rawfields, 1, 'WAVELENGTH_RANGE_UPPER_LIMIT','double');
			ob.parms.WAVELENGTH_RANGE_UPPER_LIMIT = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'DATA_ASSIMILATE_MEDIUM_MEASUREMENTS','double');
			ob.parms.DATA_ASSIMILATE_MEDIUM_MEASUREMENTS = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'DATA_ELIMINATE_SATURATED_COLORS','double');
			ob.parms.DATA_ELIMINATE_SATURATED_COLORS = val;
			
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_REFRACTIVE_INDEX','double');
			ob.parms.PARAMETERIZATION_REFRACTIVE_INDEX = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_SCATTER_MODEL','double');
			ob.parms.PARAMETERIZATION_SCATTER_MODEL = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_INPUT_MODE','string');
			ob.parms.PARAMETERIZATION_INPUT_MODE = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_INPUT_GEOMETRY_REFL','string');
			ob.parms.PARAMETERIZATION_INPUT_GEOMETRY_REFL = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_INPUT_GEOMETRY_TRAN','string');
			ob.parms.PARAMETERIZATION_INPUT_GEOMETRY_TRAN = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_WHICH_SAMPLES','string');
			ob.parms.PARAMETERIZATION_WHICH_SAMPLES = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_FIT1_WEIGHT_BW','double');
			ob.parms.PARAMETERIZATION_FIT1_WEIGHT_BW = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PARAMETERIZATION_INKSMOOTHNESS','double');
			ob.parms.PARAMETERIZATION_INKSMOOTHNESS = val;
			
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PREDICTION_OUTPUT_FILE_NAME','string');
			ob.parms.PREDICTION_OUTPUT_FILE_NAME = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PREDICTION_OUTPUT_MODE','string');
			ob.parms.PREDICTION_OUTPUT_MODE = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PREDICTION_OUTPUT_GEOMETRY','string');
			ob.parms.PREDICTION_OUTPUT_GEOMETRY = val;
			[row,val] = ob.searchTag(ob.rawfields, 1, 'PREDICTION_DELTAE_FILENAME','string');
			ob.parms.PREDICTION_DELTAE_FILENAME = val;
			
			
			
			clear('ob.rawfields');
			
			
			%res = ob.parms;
			res = 0;
		end % function
		
		
		
		%********************************************************
		% Reads the raw fields, separated by white spaces, from the text file. 
		% maxNumCols specifies the maximum allowed number of field columns
		function res = read_textfile_fields(ob,maxNumCols,delimiter)
			
			fid = fopen(ob.filename);
			
			if fid < 0
				%disp(['ERROR: File ',ob.filename,' could not be opened!']);
				%res = -1;
				%return;
				throw(MException('rh_textfile:FileNotExist', ...
					['The file "',ob.filename,'" could NOT be opened for READING!']));
			end
			
			%maxNumCols = 2;
			tex='%s';
			for i=2:(maxNumCols)
				tex = [tex,' %s'];
			end
			
			%mydata = textscan(fid, tex);
			if nargin <3
				ob.rawfields = textscan(fid, tex);
			else
				ob.rawfields = textscan(fid, tex,'Delimiter',delimiter,'MultipleDelimsAsOne',1);
			end
			fclose(fid);
			
			res = 0;
		end
		
		
		%********************************************************
		% appends a textline to a text file
		function res = write_to_text_file(ob,textarray, headerline)
			
			%dateiname = ob.modify_filename(ob.filename,'',true);
			dateiname = ob.filename;
			disp(dateiname);
			
			
			fileID = fopen(dateiname,'a');
			pos = ftell(fileID);
			if pos == 0	% if empty file, write header line...
				fprintf(fileID,'%s\n',headerline);
			end
			
			%len = length(textarray)
			formatSpec = '%s\n';
			fprintf(fileID,formatSpec,textarray);
			
			fclose(fileID);
			
			res=0;
		end
		
		
		
	end % methods
	
	
	%#############################################################
	methods (Static = true) 
	
		%********************************************************
		% searches for a given tagname (such as in CGATS text files), starting at
		% startrow, and returns its row number and value
		% if tagtype = 'double', the tag value is interpreted as such
		% if tagtype = 'string', the tag value is intepreted as string, using the
		% characters between two double quotes ("..."), if existant. If the string
		% value contains white spaces, double quotes should be used.
		function [rr,val] = searchTag(mydata,startrow,tagname,tagtype)
			%rr = startrow;
			nn = length(tagname); % Bei txt-Files mit cr/lf kann ein extra-Zeichen am Tag h�ngen!
			fertig = false;
			len = length(mydata{1}); % Number of lines from parameter text file

			%while ~fertig
			for rr = startrow:len
				tmp = mydata{1}{rr}; % col, row
				if strncmp(tmp,tagname,nn)
					tmp2 = mydata{2}{rr};
					%disp(['Feld: ',num2str(rr),' ',tmp,' ',tmp2]);
					if strcmp(tagtype,'double')
						val = str2double(tmp2); % str2double is faster than str2num
					elseif strcmp(tagtype,'string')
						quotepos = strfind(tmp2,'"');
						if ~isempty(quotepos)
							val = tmp2((quotepos(1)+1):(quotepos(end)-1));
						else
							val = tmp2;
						end
					else
						val = '### unknown type ###';
					end
					return
				end
% 				if rr > 2000   
% 					fertig=true;
% 				end
% 				rr=rr+1;
			end % for
			rr = 0; % not found
			if strcmp(tagtype,'string')
				val='';
			else
				val= 0;
			end
			
		end
		

		
	end % static methods
end % classdef