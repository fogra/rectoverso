%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

classdef rh_CGATS < rh_textfile
	% Simple and preliminary object for reading and writing ISO/CGATS measurement files
	% Writes only SAMPLE_ID, CMYK and spectral data
	
	properties %#############################################################
		numFields	= -1;
		numSets		= -1;
		
		Mode		= '';
		Aperture	= -1;
		
		%filename
		dev		% Device data CMYK
		sampName% Sample name tag
		spec	% Spectral data
		lambda	% start/end/delta/nsamp
        
        spectral_tag = 'SPECTRAL'
		
	end
	
	methods %#############################################################
		% Constructor ********************************************
		function ob = rh_CGATS(filename)
			ob@rh_textfile(filename); % superclass constructor must be explicitely called
			
			ob.lambda.zeroPadding = false;
			%ob.filename		= filename;
			
			
			%res = ob.read_CGATS_file(filename, trgLambda);
		end
		
		%{
		%Destructor ******************************************** 
		function delete(ob)
		end
		%}
		
		
		%********************************************************
		function res = copy_CGATS(ob,src)
			ob.numFields	= -1; % Es wurde ggf. nicht alles gelesen
			ob.numSets		= src.numSets;
			% filename wird nicht kopiert
			ob.dev			= src.dev;
			ob.spec			= src.spec;
			ob.lambda		= src.lambda;
			res = 0;
		end
		
		
		%********************************************************
		function res = write_CGATS_file(ob)
			
			dateiname = ob.modify_filename(ob.filename,'',true);
			disp(dateiname);
			ob.filename = dateiname; % will later be saved
			
			% Header zusammenbauen...
			ob.numFields = 1 + 4 + ob.lambda.nsamp; % ID + dev + spec
			dataFormatString = '';
			for i=1:ob.lambda.nsamp
				lam = ob.lambda.start + (i-1)*ob.lambda.delta;
				dataFormatString = [dataFormatString,['	SPECTRAL_',num2str(lam)]];
			end
			
			C = {'ISO28178';'ORIGINATOR			"Research Herzog Matlab Tool"';...
				'CREATED				"Today"';'';...
				'INSTRUMENTATION		"Mathematical Prediction"';...
				'MEASUREMENT_SOURCE	"Illumination=D50 ObserverAngle=2degree WhiteBase=Abs Filter=No"';...
				'TARGET_TYPE			"ISO12642-2"';'';...
				['NUMBER_OF_FIELDS	',num2str(ob.numFields)];'';...
				'BEGIN_DATA_FORMAT';...
				['SAMPLE_ID	CMYK_C	CMYK_M	CMYK_Y	CMYK_K',dataFormatString];...
				'END_DATA_FORMAT';'';...
				['NUMBER_OF_SETS		',num2str(ob.numSets)];'';...
				'BEGIN_DATA'};
			
			%fileID = fopen('celldata.txt','w');
			fileID = fopen(dateiname,'w');
			formatSpec = '%s\n';
			[nrows,ncols] = size(C);
			for row = 1:nrows
				fprintf(fileID,formatSpec,C{row,:});
			end
			
			% Jetzt die Daten...
			for row = 1:ob.numSets
				fprintf(fileID,'A%i',row);
				for ii = 1:4
					fprintf(fileID,'	%1.2f',ob.dev(row,ii));
				end
				for ii = 1:ob.lambda.nsamp
					fprintf(fileID,'	%1.5f',ob.spec(row,ii));
				end
				fprintf(fileID,'\n');
			end
			fprintf(fileID,'END_DATA\n');
			fclose(fileID);
		end
		
		
		%********************************************************
		function res = read_CGATS_file(ob, trgLambda, doDisplay)
			if doDisplay, disp(['Loading ',ob.filename,'...']); end
			
			maxNumFields = 56;
			%{
			fid = fopen(ob.filename);
			
			if fid < 0 
				disp(['ERROR: File ',ob.filename,' could not be opened!']);
				res = -1;
				return;
			end
			tex='%s';
			for i=1:maxNumFields
				tex = [tex,' %s'];
			end
			mydata = textscan(fid, tex);
			fclose(fid);
			%}
			ob.read_textfile_fields(maxNumFields); % reads the raw text fields to the object; max maxNumFields columns
			
			row = 1; % startrow
			[row,val] = ob.searchTag(ob.rawfields, row, 'BARBIERI_INFO_0','string');
			if row > 0
				pos = strfind(val,'Mode=');
				if pos > 0
					pos = pos + length('Mode=');
					pos2 = strfind(val,',')-1;
					ob.Mode = val(pos:pos2(1));
				end
				val = val(pos2+2:end);
				pos = strfind(val,'Aperture=');
				if pos > 0
					pos = pos + length('Aperture=');
					pos2 = strfind(val,',')-1;
					ob.Aperture = str2num(val(pos:pos2(1)));
				end
			
			else
				row=1; %not found. start for the rest with row 1
			end
			
			[row,val] = ob.searchTag(ob.rawfields, row, 'NUMBER_OF_FIELDS','double');
			if doDisplay, disp(['	Row: ',num2str(row),' NUMBER_OF_FIELDS: ',num2str(val)]);end
			ob.numFields = val;
			if ob.numFields > maxNumFields
				exc = MException('rhCGATSreader:OutOfBounds', ...
					['NUMBER_OF_FIELDS (',int2str(ob.numFields),') is beyond the allowed limits']);
				throw(exc);
			end
			[row,val] = ob.searchTag(ob.rawfields, row+1, 'BEGIN_DATA_FORMAT','double');
			DataFormatRow = row+1;

			
			[row,val] = ob.searchTag(ob.rawfields, row+1, 'NUMBER_OF_SETS','double');
			if doDisplay,  disp(['	Row: ',num2str(row),' NUMBER_OF_SETS: ',num2str(val)]);end
			%old_numSets = ob.numSets; % in order to be able to append data
			ob.numSets = val;
			
			[row,val] = ob.searchTag(ob.rawfields, row+1, 'BEGIN_DATA','double');
			DataStartRow = row+1;
			%ob.dev = 4711;
			
			tags = {'CMYK_C', 'CMYK_M', 'CMYK_Y', 'CMYK_K'};
			idldev = ob.analyzeDataFormat(ob.rawfields, DataFormatRow, tags);
			res = ob.readData('dev', idldev, ob.rawfields, DataStartRow);
			
			idlSamp = ob.analyzeDataFormat(ob.rawfields, DataFormatRow, {'SAMPLE_NAME'});
			res = ob.readData('SAMPLE_NAME', idlSamp, ob.rawfields, DataStartRow);
			
			tags = {ob.spectral_tag};
			idlspec = ob.analyzeDataFormat(ob.rawfields, DataFormatRow, tags);
			if ~isempty(idlspec) 
				ob.lambda.delta = 10;
				ob.lambda.nsamp = (ob.lambda.end - ob.lambda.start) / ob.lambda.delta +  1;
				if ob.lambda.nsamp ~= length(idlspec)
					exc = MException('rhCGATSreader:NSamples', ...
						['Number of spectral samples to be read does not match the available ones']);
					throw(exc);
				end
				res = ob.readData('spec', idlspec, ob.rawfields, DataStartRow);

				res = ob.validateLambdaRange(trgLambda);
				if res < 0
					rufeirgendeine();
				end
			end
			
% 			if old_numSets > 0 
% 				ob.numSets = ob.numSets + old_numSets; % we did append data 
% 			end
			
			clear('ob.rawfields'); 
			
			res = 0;
		end
		
		
		%********************************************************
		function res = strip_duplicates(ob,sType)
			res = 0;
			disp('	* Stripping Duplicates...');
			
			% Als weitere Optimierung k�nnte man sich das Umkopieren sparen, und
			% statt dessen am Ende nochmal sortieren.
			res = 0;
			if strcmp(sType,'dev')
				sortTmp = [ob.dev, ob.spec];
				leftCols = 4;
			elseif strcmp(sType,'sampName')
				% Zuerst cell array in numerisches Array umwandeln...
				leftCols = 0;
				for ii=1:ob.numSets % Max. Stringl�nge bestimmen...
					dtmp=uint8(cell2mat(ob.sampName(ii)));
					len=length(dtmp);
					if len>leftCols, leftCols=len; end
				end
				paddCols = zeros(ob.numSets,leftCols);
				for ii=1:ob.numSets % Strings als Zahl-Array mit Zero Padding
					dtmp=[uint8(cell2mat(ob.sampName(ii))),zeros(1,leftCols)];
					paddCols(ii,:) = dtmp(1:leftCols);
				end
				sortTmp = [paddCols, ob.spec];
			else
				throw(exc);
			end
			sortTmp = sortrows(sortTmp);
			
			i2 = 1;
			%for i1=2:ob.numSets
			i1 = 1;
			while i1 < ob.numSets % for-Schleife l�sst keine manuellen index-�nderungen zu
				i1 = i1+1;
				%if sortTmp(i1,1:4) ~= sortTmp(i2,1:4)
				cond = (sortTmp(i1,1:leftCols) == sortTmp(i2,1:leftCols) );
				%if (	sortTmp(i1,1) ~= sortTmp(i2,1) || ...
				%		sortTmp(i1,2) ~= sortTmp(i2,2) || ...
				%		sortTmp(i1,3) ~= sortTmp(i2,3) || ...
				%		sortTmp(i1,4) ~= sortTmp(i2,4)	)
				if min(cond) == 0	% Dann ist mindestens einer verschieden...
					i2 = i2+1;
					if i1 > i2 % pgh 11.2.2016
						sortTmp(i2,:) = sortTmp(i1,:);
						sortTmp(i1,1:end) = -1; % <-- sp�ter entfernen, nur f�r Debugging
					end
					continue
				end
				numDup = 2;
				cmyk2 = sortTmp(i2,1:leftCols);
				sortTmp(i2,1:end) = sortTmp(i2,1:end) + sortTmp(i1,1:end); % von 1 auf 5 �ndern!
				sortTmp(i1,1:end) = -1; % <-- sp�ter entfernen, nur f�r Debugging
				cond = (sortTmp(i1+1,1:leftCols) == cmyk2);
				while (i1 < ob.numSets) && (min(cond)==1) % alle gleich
					i1 = i1+1;
					numDup = numDup+1;
					sortTmp(i2,1:end) = sortTmp(i2,1:end) + sortTmp(i1,1:end); % von 1 auf leftCols �ndern!
					sortTmp(i1,1:end) = -1; % <-- sp�ter entfernen, nur f�r Debugging
					if i1 < ob.numSets, cond = (sortTmp(i1+1,1:leftCols) == cmyk2); end
				end
				
				sortTmp(i2,1:end) = sortTmp(i2,1:end) / numDup;
				
			end 
			
			
			if strcmp(sType,'dev')
				ob.dev = sortTmp(1:i2,1:leftCols);
				ob.sampName = [];
			elseif strcmp(sType,'sampName')
				ob.dev = [];
				
				ob.sampName = mat2cell( char(sortTmp(1:i2,1:leftCols)), ones(1,i2) );
			end
			ob.spec = sortTmp(1:i2,(leftCols+1):end);
			ob.numSets = i2;
		end
		

		
		%********************************************************
		function res = write_IT874_visual(ob)
			% Collects all data necessary for an IT874 file with visual layout and writes
			% it out as a new file. If the file already exists, creating the file is
			% skipped.
			% Currently experimental, inefficient code. Can be sped up.
		
			res = 0;
			IT874_reffile = 'data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt';
			disp(['	* Collecting ',IT874_reffile,'...']);
			
			ob2 = rh_CGATS(IT874_reffile);
			res = ob2.read_CGATS_file(ob.lambda,false);
			if res<0
				disp('*** Writing IT874-visual file failed! ***');
				return;
			end
			%ob2.filename = [ob.filename,'_vis'];
			ob2.filename = ob.modify_filename(ob.filename,'_vis',false);
			
			if exist (ob2.filename,'file'); return; end
			
			
			if length(ob.sampName) < 1, do_sampName = false; 
			else						do_sampName = true;
			end
			
			for i2=1:ob2.numSets
				
				found = false;
				for i1=1:ob.numSets
					if ob.dev(i1,:) == ob2.dev(i2,:)
						%existiert 
						if do_sampName, ob2.sampName(i2) = ob.sampName(i1); end
						ob2.spec(i2,:) = ob.spec(i1,:);
					
						found = true;
						break;
					end
                end
                
                if ~found
                    disp('*** Sample not found, cannot create IT874-visual file! ***');
                    return
                end
			end 
			ob2.lambda = ob.lambda;
			
			disp(['--> writing CGATS file ',ob2.filename]);
			ob2.write_CGATS_file();
			disp('	Done writing file.');
		end

			
		%********************************************************
		function idl = analyzeDataFormat(ob, mydata, row, tags)
			% returns the index list of the desired fields; 
			% index = 0 means "not found"
			idl = [];
			len = length(tags);
			if strcmp(tags(1), ob.spectral_tag)
				if len > 1
					exc = MException('rhCGATSreader:Spectral', ...
						['SPECTRAL data cannot be read alongside with other data!']);
					throw(exc);
				end
				l1 = length(ob.spectral_tag);
				l2 = length('NM_');
				tt = 0; % count of tags
				ob.lambda.start = 1000;
				ob.lambda.end	= 0;
				for cc=1:ob.numFields
					tmp = mydata{cc}{row};
					if strncmp(tmp,ob.spectral_tag,l1) || strncmp(tmp,'NM_',l2)
						tt = tt+1;
						idl(tt) = cc;
						
						lamStr = strrep(tmp,'SPECTRAL_','');
						lamStr = strrep(lamStr,'NM_',''); % exists sometimes, e.g. SPECTRAL_NM_380
						%lam = str2num(lamStr);
						lam = str2double(lamStr);
						
						if lam < ob.lambda.start
							ob.lambda.start = lam;
						end
						if lam > ob.lambda.end
							ob.lambda.end = lam;
						end
						
					end
				end
				return;
			end
			idl = zeros(1,len);
			%found = false;
			for cc=1:ob.numFields
				tmp = mydata{cc}{row};
				
				for tt=1:len
					if strcmp(tmp,tags(tt))
						idl(tt) = cc;
						%found = true;
						break;
					end
				end
			end
			
		end

			
		%********************************************************
		function res = readData(ob,which,idl,mydata,startRow)
			if sum(idl) <= 0 % none of the fields is available
				res = -1;
				return;
			end
			len = length(idl);
			endRow = startRow+ob.numSets-1;
			if strcmp(which, 'SAMPLE_NAME') % Text Data...
				colarray = cell(ob.numSets,len);
				for ii=1:len
					%for rr=1:ob.numSets
						if idl(ii) <= 0, continue, end
						tmp = mydata{idl(ii)};
						tmp2 = tmp(startRow:endRow);
						colarray(:,ii) = tmp2;
					%end
				end
			else % Numerical Data...
				colarray = zeros(ob.numSets,len);
				for ii=1:len
					if idl(ii) <= 0, continue, end
					for rr=1:ob.numSets
						tmp = mydata{idl(ii)}{startRow+rr-1};
						%ob.dev(rr,ii) = str2num(tmp);
						colarray(rr,ii) = str2double(tmp);
					end
				end
			end
			if strcmp(which, 'dev')
				ob.dev = colarray;
				%ob.dev = [ob.dev; colarray]; % we append loaded data
			elseif strcmp(which, 'SAMPLE_NAME')
				ob.sampName = colarray;
				%ob.sampName = [ob.sampName; colarray]; % we append loaded data
			elseif strcmp(which, 'spec')
				ob.spec= colarray;
				%ob.spec = [ob.spec; colarray]; % we append loaded data
			else
				res = -1;
				return;
			end
			res = 0; % everything OK
		end
		
		
		%********************************************************
		function res = validateLambdaRange(ob,trgLambda)
			if trgLambda.start < 1 
				res = 0; 
				return
			end
			if (trgLambda.start ~= ob.lambda.start ||...
					trgLambda.delta ~= ob.lambda.delta  )
				res = -1; % schwer zu korrigieren
				return
			elseif (ob.lambda.end > trgLambda.end)
				res = +1; % wir haben mehr daten als wir brauchen
				return;
			elseif ( ob.lambda.end	< trgLambda.end )
				addsamp = (trgLambda.end - ob.lambda.end)/trgLambda.delta;
				
				% Padding of missing samples: It does not make sense to pad with zeros,
				% as a model may run into trouble with 0/0 calculations. Reasons:
				% (1) It appears logic that an inkless patch (paper) has higher
				% transmission/reflection than a printed patch.
				% (2) It turns out that the spectra of those measurements that do
				% have enough samples up to 780nm show increasing values at this wavelength end.
				% Hence, the error is less if the last available real sample is used for
				% padding than if zero ist used.
				for nn=1:ob.numSets
					padval = ob.spec(nn,ob.lambda.nsamp);
					for ii=1:addsamp
						ob.spec(nn,ob.lambda.nsamp+ii) = padval;
					end
					% the other spectra automatically get the new size with 0
					% padding
				end
				ob.lambda.end = trgLambda.end;
				ob.lambda.nsamp = ob.lambda.nsamp + addsamp;
				ob.lambda.zeroPadding = true;
			end
			res = 0; % alles gut
		end
		
		
		
		%********************************************************
		% Checks if the measurement data have all the required SAMPLE_NAMEs. 
		% If not, it assumes that the existing samples equally distribute among the
		% expected sample names in the correct order.
		function res = ensure_samplenames(ob, namelist,meas_mode)
			if strcmp(meas_mode,'tran')
				modecmp = 'refl';
			elseif strcmp(meas_mode,'refl')
				modecmp = 'tran';
			else
				throw(MException('rh_CGATS:MeasModeUnknown', ['Measurement Mode unknown!']));
			end
			if strfind(ob.Mode,modecmp)
 				throw(MException('rh_CGATS:WrongMeasMode', ['Measurement Mode of file is different than expected!']));
			end

			veriflist = ones(1,ob.numSets); % m�ssen alle abgehakt werden
			namecnt = length(namelist);
			for ii=1:ob.numSets
				tmp = ob.sampName(ii);
				for jj=1:namecnt
					if strcmp(tmp,namelist(jj))
						veriflist(ii) = 0;
					end
				end
			end
			if sum(veriflist) > 0
% 				prompt = {['Not all of the sampleNames of :',ob.filename,'could be recognized.\n\r',...
% 					num2str(ob.numSets),' samples are provided, and ',num2str(namecnt),' sampleNames are expected.\n'...
% 					'Is it correct to assume?']};
% 				dlg_title = 'Input for sampleNames';
% 				num_lines = 1;
% 				def = {'20'};
% 				answer = inputdlg(prompt,dlg_title,num_lines,def);
				frequency = ob.numSets / namecnt;
				freqint = idivide(int32(ob.numSets), namecnt);
				if ~frequency==freqint 
					throw(MException('rh_CGATS:WrongSampleCount', ...
						['The current number of samples in the file is not as expected!']));
				end
				for jj=1:namecnt
					for ii=1:freqint
						ob.sampName((jj-1)*freqint+ii) = namelist(jj);
					end
				end
				disp(['*** Assigned ',namelist]);
			end
		end
		
		
		%********************************************************
		function res = append_samples(ob, CGtmp)
			for ii=1:CGtmp.numSets
				if ~isempty(ob.dev),		ob.dev(ob.numSets+ii,:)		= CGtmp.dev(ii,:);		end
				if ~isempty(ob.sampName),	ob.sampName(ob.numSets+ii)	= CGtmp.sampName(ii);	end
				if ~isempty(ob.spec),		ob.spec(ob.numSets+ii,:)	= CGtmp.spec(ii,:);		end
			end
			ob.numSets = ob.numSets + CGtmp.numSets;
			if ~strcmp(ob.Mode,CGtmp.Mode),	ob.Mode='Mixed';	end
			if ob.Aperture~=CGtmp.Aperture,	ob.Aperture=0;		end
		end
		
	end
	
	methods (Static = true) %#############################################################
		
		%********************************************************
		function dateiname = modify_filename(oldname,add1,withExCheck)
			% appends an additive text to the end of a filename BEFORE the extension
			dotpos = strfind(oldname,'.');
			if ~isempty(dotpos)
				rumpfname = oldname(1:(dotpos(end)-1));
				ext = oldname((dotpos(end)+1):end);
			else
				rumpfname = oldname;
				ext = '';
			end
			rumpfname = [rumpfname,add1];
			neuok = false;
			ii = 0;
			add2 = '';
			while ~neuok
				dateiname = [rumpfname,add2,'.',ext];
				ii = ii+1;
				if withExCheck, res = exist (dateiname,'file');
				else			res = 0;
				end
				if res>0
					add2 = ['-',num2str(ii)];
				else
					neuok = true;
				end
			end
			
			
		end
	end
	
	
end