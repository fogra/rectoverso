%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

function doit()

do_the_refl2tran();

end



%********************************************************
% Contains the necessary steps in order to predict transparency measurements from
% reflectance measurements in the course of the Fogra project 10.060 "Farbmanagement
% f�r transparente Medien"
function do_the_refl2tran()

%### 0. Init the model object
%cm = rh_RectoVerso(); % Constructor for Recto-Verso Color Model
%cm = rh_RectoVerso_dbg(); % Constructor for Recto-Verso Color Model
cm = rh_RectoVerso_report(); % Constructor for Recto-Verso Color Model



%### 1. Data Loading and Analysis

% Load the parameter file...
dataFolder = 'data/'; % Folder of parameter file; not necessarily equal to COLOR_DATA_FOLDER
paramFileName = [dataFolder, '01_RectoVerso_params.txt'];
% paramFileName = [dataFolder, 'HP_Polyester_RectoVerso_params.txt'];

% Load parameters
parob = rh_textfile(paramFileName);

% parameters are read to cm.parm
res = parob.read_text_file(true); 
if res == 0
	cm.parm= parob.parms;
end
clear ('parob');

% ### Now run a loop over all the scatter models in PSMlist...
% ### If only the scatter model from the parameter file is desired, set PSMlist to [cm.parm.PARAMETERIZATION_SCATTER_MODEL]
PSMlist = [12];% 2 1 0];
% PSMlist = [0];
% PSMlist = [12 2 1 0];
% PSMlist = [cm.parm.PARAMETERIZATION_SCATTER_MODEL];
for ip=1:length(PSMlist) % Alles durchnudeln mit verschiedenen Scattermodels...
	cm.parm.PARAMETERIZATION_SCATTER_MODEL = PSMlist(ip);
	
	% The following parstr is used for file naming of the saved figures and the dE
	% output in the text file
	cm.parm.parstr = [...
		'PSM=',num2str(cm.parm.PARAMETERIZATION_SCATTER_MODEL),';',...
		'ISM=',num2str(cm.parm.PARAMETERIZATION_INKSMOOTHNESS),';',...
		'PWS=',cm.parm.PARAMETERIZATION_WHICH_SAMPLES,';',...
		'AMM=',num2str(cm.parm.DATA_ASSIMILATE_MEDIUM_MEASUREMENTS),';',...
		'ESC=',num2str(cm.parm.DATA_ELIMINATE_SATURATED_COLORS),...
		';','WBW=',num2str(cm.parm.PARAMETERIZATION_FIT1_WEIGHT_BW),...
		];
	pfad = [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER,'graphics/','Diary',cm.parm.parstr,'.txt'];
	diary(pfad); % saves all the screen output in a file, in order to have more information available
	
	res = cm.load_measData(); % refl., trans., white backing
	
	%### 2. Parameterizing the Hebert Recto-Verso Model
	
	%  load 'zustand_Hebert_calibrate'
	%  cm.plot_paperparms();
	% cm.plot_tu_Neugebauer();
	
	dbgflag.load = false; % ############# <<<<<<<<<<<<< ############
	
	if dbgflag.load
		load 'zustand_Hebert_calibrate'
		%p1=0;	p2=0;	p3=0;	p4=0;
	else
		%cm.calc_TVI('refl');
		%cm.calc_TVI('tran');
		cm.analyse_data();
	end
	
	
	cm.Hebert_calibrate(dbgflag);
    
	%save zustand_Hebert_calibrate cm 
    % F�r Debugging. Damit man nicht immer von vorne anfangen muss
	cm.save_state();
	
	cm.plot_tu_Neugebauer(1,4,121);
	cm.plot_tu_Neugebauer(5,8,122);
	cm.plot_tu_Neugebauer(9,12,123);
	cm.plot_tu_Neugebauer(13,16,124);
	
	%### 3. Calculating the Output using the Recto-Verso Model
	cm.outData = rh_CGATS([cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER, cm.parm.PREDICTION_OUTPUT_FILE_NAME]);

    cm.outData.dev		= cm.reflData.dev;
	cm.outData.lambda	= cm.reflData.lambda;
	
	cm.Hebert_predict_solid_colors(cm.parm.PREDICTION_OUTPUT_GEOMETRY,cm.parm.PREDICTION_OUTPUT_MODE,cm.Hebert.tu);
	
	%### 4. Writing Data back to Disk
	
    len = size(cm.outData.spec, 1);
	cm.outData.numSets	= len;
	
	cm.outData.write_CGATS_file();
	
	
	%### 5. Calculate dE-results...

    dE = cm.calculate_dEresults();
	flags.plotBest = 0;
	
	cm.plot_resultSpectra(dE,flags,131);
	
	% Prepare info for saving with the results...
	pfad = [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER];
	if strfind(cm.outData.filename,pfad)
		len = length(pfad);
		dateiname = cm.outData.filename(len+1:end);
	else
		dateiname = cm.outData.filename;
	end
	dEstring = [char(datetime),'	',num2str(dE.mean),'	',num2str(dE.medi),'	',num2str(dE.quant95), '	', num2str(dE.max),'	',num2str(dE.std),'	','{',...
		...% 	'PSM=',num2str(cm.parm.PARAMETERIZATION_SCATTER_MODEL),';',...
		...% 	'ISM=',num2str(cm.parm.PARAMETERIZATION_INKSMOOTHNESS),';',...
		...% 	'PWS=',cm.parm.PARAMETERIZATION_WHICH_SAMPLES,';',...
		...% 	'AMM=',num2str(cm.parm.DATA_ASSIMILATE_MEDIUM_MEASUREMENTS),';',...
		...% 	'ESC=',num2str(cm.parm.DATA_ELIMINATE_SATURATED_COLORS),';',...
		cm.parm.parstr,...
		';',dateiname,';',...
		'',cm.parm.REFLECTANCE_MEASUREMENTS_FILE_NAME,';',...
		'',cm.parm.TRANSMISSION_MEASUREMENTS_FILE_NAME,';',...
		'',cm.parm.MEDIUM_MEASUREMENTS_FILE_NAME,';',...
		'}'];   
	headerline = 'Date				dEmean	dEmedi	dEquant95 dEmax	dEstd	Parameters';
	dEob = rh_textfile([cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER,cm.parm.PREDICTION_DELTAE_FILENAME]);
	% Appends the results to the file
    res = dEob.write_to_text_file(dEstring, headerline);
	clear('dEob');
	diary off;
	%cm.plot_scatterCorr(dE,141);
	load splat.mat;		sound(y,1*Fs);
end % for

end

