%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

classdef rh_RectoVerso_report < rh_RectoVerso
	%rh_RectoVerso_report contains all the functions for display and evaluation of
	%data, e.g. plots
	
	properties
	end
	
	methods
		% Constructor ********************************************
		function cm = rh_RectoVerso_report()
		end
		
		
		%********************************************************
		function fig = plot_TVI(cm,istart,iend,fig0,lineThick)
			if fig0 < 1
				fig=figure('Position',[400,570,560,420]); % Default-Gr��e: 560,420;
			else
				fig=figure(fig0);
			end
			if strcmp(lineThick,'thick')
				newplot(fig); 
			end
			
			grid on;
			%axis([cm.lambda.start,cm.lambda.end,0,inf]);
			labels = {};
			colors = rh_color.cmyk2rgb_simplex(cm.colorants.dev)*0.9;
			title(['TVI- ',cm.parm.MEDIUM_SUBFOLDER], 'Interpreter', 'none');
			hold on;
			
			len = 16;
			% First, the tu predicted from refl...
			for ii=1:len
				if ii<istart || ii>iend,	continue,	end % �berspringen
				if sum(colors(ii,:)) > 2.7
					colors(ii,:) = colors(ii,:) - .7;
					linestyle = '--';
				else
					linestyle = 'o-';
				end
				if ii>9,	
					linestyle = 'o-';
					fak = 0.5/max(colors(ii,:));
					if fak > 4 
						colors(ii,:) = colors(ii,:) * 4;
					elseif fak > 1
						colors(ii,:) = colors(ii,:) * fak;
					end
				end
				labels(end+1) = cellstr([char(cm.colorantsList(ii)),' Refl']);
				
				%if fig0>0
				if strcmp(lineThick,'thick'), 
					linwid = 2;
				else
					linwid = 1;
				end
				if isempty(cm.wedgeRefl{1,ii}.dev),		continue;	end
				xx = cm.wedgeRefl{1,ii}.TVcurve(:,1);
				yy = cm.wedgeRefl{1,ii}.TVcurve(:,2);
				plot (xx, yy ,linestyle,'color',colors(ii,:), 'LineWidth',linwid,'MarkerSize',10);
				
				
				labels(end+1) = cellstr([char(cm.colorantsList(ii)),' Tran']);
				yy = cm.wedgeTran{1,ii}.TVcurve(:,2);
				plot (xx, yy,'x-','color',colors(ii,:)*.8, 'LineWidth',linwid,'MarkerSize',10);
			end
			
			legend(labels,'Location','best');
			hold off;
			
			pfad = [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER,'graphics/','TVI',num2str(istart),'-',num2str(iend),cm.parm.parstr,'.fig'];
			saveas(fig,pfad);
			pause(.1);
		end
		
		
		%********************************************************
		function fig = plot_paperparms(cm,figNo)
			[anz,~] = size(cm.dbg.HebertParms);
			
			if figNo < 1
				fig=figure('Position',[100,570,560,420]); % Default-Gr��e: 560,420;
			else
				fig=figure(figNo);
				newplot(fig);
            end
            
			grid on;
			axis([cm.lambda.start,cm.lambda.end,0,1]);
			labels = {'rho1','rho2','tau','r2t','rBw','rBb','csc/bsc','t_u'};
			colors = [0 .7 0; 0 .5 .7; .6 0 .6; .5 .5 .5; .4 .4 .7; .3 .3 .3; .7 .6 0; 0 0 0];
			title(sprintf('Paper Parameters for %s, SCATTER MODEL=%d.', ...
                          cm.parm.MEDIUM_SUBFOLDER,  ...
                          cm.parm.PARAMETERIZATION_SCATTER_MODEL),  ...
                  'Interpreter', 'none');
			
			hold on;
			lam=cm.lambda.start:cm.lambda.delta:cm.lambda.end;
			
			len = min(anz,length(labels));
			for ii=1:len
				if strcmp(labels(ii),'csc/bsc')
					bmin=0.7;	bmax=3.0;
					onelevel=(1-bmin)/(bmax-bmin);
					parpl = [cm.dbg.HebertParms(ii,1:16)*10,(cm.dbg.HebertParms(ii,17:32)-bmin)/(bmax-bmin)];
					labels(ii) = {['csc*10;bsc',num2str(bmin),'-',num2str(bmax)]};
					plot (lam(1:length(parpl)), parpl,'o','color',colors(ii,:),'LineWidth',1.7);
					iicsc = ii;
				elseif ii > 7
					plot (lam, cm.dbg.HebertParms(ii,:),'.','color',colors(ii,:), 'LineWidth',1);
				else
					plot (lam, cm.dbg.HebertParms(ii,:),'color',colors(ii,:), 'LineWidth',2);
				end
			end
			plot([540 690],[1 1]*onelevel,'color',colors(iicsc,:));
			
    		legend(labels,'Location','best');
            
			hold off;
			
			pfad = [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER,'graphics/','PaperPar',cm.parm.parstr,'.fig'];
			saveas(fig,pfad);
			pause(1);
		end
		
		
		%********************************************************
		% Plots the ink layer transmission t_u of most of the Neugebauer primaries
		% thick lines: calculated from reflection measurements
		% thin lines: calculated from transmission measurements
		function fig = plot_tu_Neugebauer(cm, istart, iend,figNo)
			if figNo < 1
				fig=figure('Position',[200,570,560,420]); % Default-Gr��e: 560,420;
			else
				fig=figure(figNo);
				newplot(fig);
			end
			grid on;
			%axis([cm.lambda.start,cm.lambda.end,0,1]);
			axis([cm.lambda.start,cm.lambda.end,0,inf]);
			labels = {};
			colors = rh_color.cmyk2rgb_simplex(cm.colorants.dev);
			title(['Ink layer t\_u ',cm.parm.MEDIUM_SUBFOLDER,' SCATTER MODEL=',num2str(cm.parm.PARAMETERIZATION_SCATTER_MODEL)])
			
			hold on;
			lam=cm.lambda.start:cm.lambda.delta:cm.lambda.end;
			
			len = 16;
			% First, the tu predicted from refl...
			for ii=1:len
				if ii<istart || ii>iend,	continue,	end % �berspringen
				if sum(colors(ii,:)) > 2.7
					colors(ii,:) = colors(ii,:) - .7;
					linestyle = '--';
				else
					linestyle = '-';
				end
				if ii>9,	
					linestyle = '.-';	
					%colors(ii,:) = colors(ii,:) * 4;
					fak = 0.5/max(colors(ii,:));
					if fak > 4 
						colors(ii,:) = colors(ii,:) * 4;
					elseif fak > 1
						colors(ii,:) = colors(ii,:) * fak;
					end
				end
				labels(end+1) = cm.colorantsList(ii);
				
				if strcmp(cm.parm.PARAMETERIZATION_WHICH_SAMPLES,'Chart')
					idx = cm.find_idx_by_devdata(cm.reflData,cm.colorants.dev(ii,:));
				else
					idx = ii;
				end
				if idx <= 0,	continue;	end
				%plot (lam, cm.Hebert.tu(ii,:),linestyle,'color',colors(ii,:), 'LineWidth',2);
				plot (lam, cm.Hebert.tu(idx,:),linestyle,'color',colors(ii,:), 'LineWidth',2);
				
				% the same tu from the first parameter-fit...
				if ii==9 && length(cm.dbg.HebertParms(:,1)) == 8 % case PARAMETERIZATION_SCATTER_MODEL==1
					labels(end+1) = cellstr([char(cm.colorantsList(ii)),' from Fit1']);
					plot (lam, cm.dbg.HebertParms(8,:),':','color',colors(ii,:), 'LineWidth',2);
				elseif ii>1 && length(cm.dbg.HebertParms(:,1)) > 8 % case PARAMETERIZATION_SCATTER_MODEL==2
					labels(end+1) = cellstr([char(cm.colorantsList(ii)),' from Fit1']);
					plot (lam, cm.dbg.HebertParms(ii+6,:),':','color',colors(ii,:), 'LineWidth',2);
				end
				% Then, the tu predicted from tran (for comparison); should be ideally identical
				labels(end+1) = cellstr([char(cm.colorantsList(ii)),' from Tran']);
				plot (lam, cm.Hebert.tu_frTr(ii,:),'x-','color',colors(ii,:), 'LineWidth',1,'MarkerSize',10);
			end
			
			% Then, the tu predicted from tran (for comparison); should be ideally identical
% 			for ii=1:len
% 				plot (lam, cm.Hebert.tu_frTr(ii,:),'x-','color',colors(ii,:), 'LineWidth',1);
% 			end
			
			legend(labels,'Location','best');
			hold off;
			
			pfad = [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER,'graphics/','tu',num2str(istart),'-',num2str(iend),cm.parm.parstr,'.fig'];
			saveas(fig,pfad);
			pause(.1);
		end
		
		
		%********************************************************
		% Plots the ink layer transmission t_u of most of the Neugebauer primaries
		% thick lines: calculated from reflection measurements
		% thin lines: calculated from transmission measurements
		function fig = plot_resultSpectra(cm,dE,flags,figNo)
			if figNo < 1
				fig=figure('Position',[300,570,560,420]); % Default-Gr��e: 560,420;
			else
				fig=figure(figNo);
				newplot(fig);
			end
			grid on;
			axis([cm.lambda.start,cm.lambda.end,0,inf]);
			labels = {};
			%colors = rh_color.cmyk2rgb_simplex(cm.colorants.dev);
			title(['Result spectra-',cm.parm.MEDIUM_SUBFOLDER,' SCATTER MODEL=',num2str(cm.parm.PARAMETERIZATION_SCATTER_MODEL)])
			
			hold on;
			lam=cm.lambda.start:cm.lambda.delta:cm.lambda.end;
			
			if flags.plotBest
				col = rh_color.cmyk2rgb_simplex(dE.best.dev);
				if sum(col) > 2.7,		col = col - 0.7;	end
				labels(end+1) = {['Best=',char(num2str(dE.best.dev))]};
				plot (lam, dE.best.spec,'-','color',col, 'LineWidth',2);
				labels(end+1) = {['Measured=',char(num2str(dE.best.dev))]};
				plot (lam, dE.best.specRef,'x-','color',col, 'LineWidth',1);
			end
			col = rh_color.cmyk2rgb_simplex(dE.median.dev);
			if sum(col) > 2.7,		col = col - 0.7;	end
			labels(end+1) = {['Median=',char(num2str(dE.median.dev)),' dE',num2str(dE.medi,'%1.1f')]};
			plot (lam, dE.median.spec,'+-','color',col, 'LineWidth',2);
			labels(end+1) = {['Measured=',char(num2str(dE.median.dev))]};
			plot (lam, dE.median.specRef,'+-','color',col, 'LineWidth',1);
			
			col = rh_color.cmyk2rgb_simplex(dE.worst.dev);
			if sum(col) > 2.7,		col = col - 0.7;	end
			labels(end+1) = {['Worst=',char(num2str(dE.worst.dev)),' dE',num2str(dE.max,'%1.1f')]};
			plot (lam, dE.worst.spec,'o-','color',col, 'LineWidth',2);
			labels(end+1) = {['Measured=',char(num2str(dE.worst.dev))]};
			plot (lam, dE.worst.specRef,'o-','color',col, 'LineWidth',1);
			
			legend(labels,'Location','best');
			hold off;
			
			pfad = [cm.parm.COLOR_DATA_FOLDER,cm.parm.MEDIUM_SUBFOLDER,'graphics/','ResSpec',cm.parm.parstr,'.fig'];
			saveas(fig,pfad);
			pause(.1);
			
		end
		
		
		
		%********************************************************
		function y = plot_scatterCorr(cm,dE,figNo)
			if figNo < 1
				fig=figure('Position',[400,570,560,420]); % Default-Gr��e: 560,420;
			else
				fig=figure(figNo);
				newplot(fig);
			end
			
			grid on;
			axis([cm.lambda.start,cm.lambda.end,0,inf]);
			%axis([0,inf,0,inf]);
			labels = {};
			title(['TODO ']);			
			hold on;
			
			lam=cm.lambda.start:cm.lambda.delta:cm.lambda.end;
			labels(end+1) = {'Anfang'};
			
			idx0 = dE.worst.idx;
			dev_ = cm.Hebert.dev(idx0,:); % <-- PRoblem !!! Dimension stimmt nicht
			
			% worst.spec
			%col = [1 0 0];
			%plot (lam, dE.worst.spec,'x-','color',col, 'LineWidth',1);
			
			% outData.spec
			col = [0 1 0];
			%plot (lam, cm.outData.spec(idx0,:),'+-','color',col, 'LineWidth',1);
			
			% inData refl-Spec
			idx_in = cm.find_idx_by_devdata(cm.reflData,dev_);
			spec = cm.reflData.spec(idx_in,1:cm.lambda.nsamp);
			
			plot (lam, spec,'+-','color',col, 'LineWidth',1);
			
			
			% 
			col = [0 1 1];
			rho1 = cm.Hebert.rho1;
			rho2 = cm.Hebert.rho2;
			tau  = cm.Hebert.tau;
			rBw = cm.Hebert.rBw;
			[csc,bsc] = cm.Hebert_sc_par(dev_);
			%[csc,bsc] = cm.Hebert_sc_par(dE.worst.dev);
			tu = cm.Hebert.tu(idx0,:);
			%tu = cm.Hebert.tu_frTr(idx0,:);
			r2t = cm.Hebert.r2t;
			%pred = norm_T * cm.Hebert.TpRId0_f(rho1,rho2,tau,cm.Hebert.r_10,r2t,tu);
			%pred = cm.Hebert.T_RId0_f(rho1,rho2,tau,cm.Hebert.r_10,r2t,tu);
					
			pred = cm.Hebert.R_RI45_f(rho1,rho2,tau,cm.Hebert.r_10,rBw,0,tu,1);
			plot (lam, pred,'o-','color',col, 'LineWidth',2);
			
			%spec = cm.Hebert.R_RI45_f(cm.Hebert.rho1,cm.Hebert.rho2,cm.Hebert.tau,cm.Hebert.r_10,cm.Hebert.rBw,...
			%	csc,cm.Hebert.tu(idx0,:),bsc);
			
			col = [0 0 1];
			pred = cm.Hebert.R_RI45_f(rho1,rho2,tau,cm.Hebert.r_10,rBw,csc,tu,bsc);
			plot (lam, pred,'o-','color',col, 'LineWidth',2);
			
			
			legend(labels);
			hold off;
			
			pause(.1);
			
		end
		
		
		
	end
	
	
	methods (Static = true) %#############################################################
		
		function fig = plot_scatterfuncs(plus2)
			sc_f1 = @(b,c,x)	x .* (1-c) + c;
			sc_f2 = @(b,c,x)	(1-c-b).*x.^2 + b.*x + c;
			
			cc = 0.1;
			
			fig=figure;
			hold
			grid
			axis square
			title('Scatter Correction Function')
			
			xx = 0:.01:1;
			plot(xx,sc_f1(1,cc,xx),'-','color',[0 0 1], 'LineWidth',2);
			plot(xx,xx,'-','color',[0 0 0], 'LineWidth',1);
			if plus2
				plot(xx,sc_f2(1.7,cc,xx),'-','color',[0 1 0], 'LineWidth',2);
				plot(xx,sc_f2(.25,cc,xx),'-','color',[0 1 0], 'LineWidth',2);
				legend({'1 parameter','2 parameters'},'Location','best');
			end
		end
		
	end
	
end

