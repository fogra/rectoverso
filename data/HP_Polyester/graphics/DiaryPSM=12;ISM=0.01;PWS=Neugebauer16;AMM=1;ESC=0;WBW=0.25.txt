Loading data/HP_Polyester/../Reference IT874-1680.txt...
	Row: 15 NUMBER_OF_FIELDS: 5
	Row: 19 NUMBER_OF_SETS: 1680
Loading data/HP_Polyester/HP_Polyester_Reflection_i1Pro2.txt...
	Row: 7 NUMBER_OF_FIELDS: 42
	Row: 11 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Transmission_Absolute.spd.txt...
	Row: 17 NUMBER_OF_FIELDS: 38
	Row: 22 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Medium.txt...
	Row: 8 NUMBER_OF_FIELDS: 38
	Row: 13 NUMBER_OF_SETS: 30
	* Stripping Duplicates...
	Found ink saturation @ C=50
	Found ink saturation @ CMY=85
	Found ink saturation @ K=50
Solving for Paper Parameters  (ca. 20s-5min)... Start @21:15:28
		### Bei lami = 99999 Exit flag: 3Squared 2-norm large: 0.86144
Elapsed time is 68.753987 seconds.
Elapsed time is 0.549484 seconds.
DONE Calculating Ink layer transmissions.
Optimizing scatter parameters...
Optimizing csc(lam) 10/16
Elapsed time is 0.691274 seconds.
		DONE Optimizing scatter parameters.
The squared 2-norm is quite large: 0.033692
Elapsed time is 0.749934 seconds.
DONE Calculating Ink layer transmissions.
Elapsed time is 0.000945 seconds.
DONE Predicting solid colorant spectra.
data/HP_Polyester/HP_Polyester_Reflection_Absolute_Prediction.spd-24.txt
*** Calculating Delta E...
	Used Colors: 16/16. Eliminated 0 saturated colors.
data/HP_Polyester/HP_Polyester_Reflection_Absolute_Prediction_dE.txt
Loading data/HP_Polyester/../Reference IT874-1680.txt...
	Row: 15 NUMBER_OF_FIELDS: 5
	Row: 19 NUMBER_OF_SETS: 1680
Loading data/HP_Polyester/HP_Polyester_Reflection_i1Pro2.txt...
	Row: 7 NUMBER_OF_FIELDS: 42
	Row: 11 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Transmission_Absolute.spd.txt...
	Row: 17 NUMBER_OF_FIELDS: 38
	Row: 22 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Medium.txt...
	Row: 8 NUMBER_OF_FIELDS: 38
	Row: 13 NUMBER_OF_SETS: 30
	* Stripping Duplicates...
	Found ink saturation @ C=50
	Found ink saturation @ CMY=85
	Found ink saturation @ K=50
<a href="matlab: opentoline('Z:\InVo\Byshko\laufend\backlit\Recto-Verso\recto-verso\rh_RectoVerso.m',255,1)">255 </a>			cm.Hebert.R_RI45_f = @(rho1,rho2,tau,r1,r2,csc,tu,bsc)		...
mu_f

mu_f =

  <a href="matlab:helpPopup function_handle" style="font-weight:bold">function_handle</a> with value:

    @(theta0,n)(1-(sin(theta0)/n)^2)^(-1/2)

mu_f(0, 1.5)

ans =

     1

cm.Hebert.T_10_0

ans =

    0.9600

rh_colorModel gel�scht
doit
Loading data/HP_Polyester_RectoVerso_params.txt...
Loading data/HP_Polyester/../Reference IT874-1680.txt...
	Row: 15 NUMBER_OF_FIELDS: 5
	Row: 19 NUMBER_OF_SETS: 1680
Loading data/HP_Polyester/HP_Polyester_Reflection_i1Pro2.txt...
	Row: 7 NUMBER_OF_FIELDS: 42
	Row: 11 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Transmission_Absolute.spd.txt...
	Row: 17 NUMBER_OF_FIELDS: 38
	Row: 22 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Medium.txt...
	Row: 8 NUMBER_OF_FIELDS: 38
	Row: 13 NUMBER_OF_SETS: 30
	* Stripping Duplicates...
	Found ink saturation @ C=50
	Found ink saturation @ CMY=85
	Found ink saturation @ K=50
<a href="matlab: opentoline('Z:\InVo\Byshko\laufend\backlit\Recto-Verso\recto-verso\rh_RectoVerso.m',254,1)">254 </a>			cm.Hebert.R_RI45_f = @(rho1,rho2,tau,r1,r2,csc,tu,bsc)		...
tu
{Undefined function or variable 'tu'.
} 
cm.Hebert.r1a_f(r1, tu)
{Undefined function or variable 'r1'.
} 
cm.Hebert.r1a_f(1, 1)

ans =

     1

cm.Hebert.r1a_f(0.596, 0:0.05:1)

ans =

  Columns 1 through 10

         0    0.0001    0.0004    0.0013    0.0030    0.0059    0.0102    0.0161    0.0242    0.0347

  Columns 11 through 20

    0.0481    0.0651    0.0863    0.1127    0.1453    0.1856    0.2356    0.2977    0.3753    0.4727

  Column 21

    0.5960

cm.Hebert.r1a_f(0.596, 0:0.05:1)'

ans =

         0
    0.0001
    0.0004
    0.0013
    0.0030
    0.0059
    0.0102
    0.0161
    0.0242
    0.0347
    0.0481
    0.0651
    0.0863
    0.1127
    0.1453
    0.1856
    0.2356
    0.2977
    0.3753
    0.4727
    0.5960

cm.Hebert.beta

ans =

    2.9446

plot(cm.Hebert.r1a_f(0.596, 0:0.05:1))
close all
plot(cm.Hebert.r1a_f(0.596, 0:0.05:1))
horzcat(plot(cm.Hebert.r1a_f(0.596, 0:0.05:1)), 0:0.05:1)
{Error using <a href="matlab:matlab.internal.language.introspective.errorDocCallback('matlab.graphics.chart.primitive.Line/horzcat')" style="font-weight:bold">matlab.graphics.chart.primitive.Line/horzcat</a>
Cannot convert double value 0.05 to a handle
} 
horzcat((cm.Hebert.r1a_f(0.596, 0:0.05:1)), 0:0.05:1)

ans =

  Columns 1 through 10

         0    0.0001    0.0004    0.0013    0.0030    0.0059    0.0102    0.0161    0.0242    0.0347

  Columns 11 through 20

    0.0481    0.0651    0.0863    0.1127    0.1453    0.1856    0.2356    0.2977    0.3753    0.4727

  Columns 21 through 30

    0.5960         0    0.0500    0.1000    0.1500    0.2000    0.2500    0.3000    0.3500    0.4000

  Columns 31 through 40

    0.4500    0.5000    0.5500    0.6000    0.6500    0.7000    0.7500    0.8000    0.8500    0.9000

  Columns 41 through 42

    0.9500    1.0000

horzcat(0:0.05:1, cm.Hebert.r1a_f(0.596, 0:0.05:1)')
{Error using <a href="matlab:matlab.internal.language.introspective.errorDocCallback('horzcat')" style="font-weight:bold">horzcat</a>
Dimensions of matrices being concatenated are not consistent.
} 
horzcat((0:0.05:1)', cm.Hebert.r1a_f(0.596, 0:0.05:1)')

ans =

         0         0
    0.0500    0.0001
    0.1000    0.0004
    0.1500    0.0013
    0.2000    0.0030
    0.2500    0.0059
    0.3000    0.0102
    0.3500    0.0161
    0.4000    0.0242
    0.4500    0.0347
    0.5000    0.0481
    0.5500    0.0651
    0.6000    0.0863
    0.6500    0.1127
    0.7000    0.1453
    0.7500    0.1856
    0.8000    0.2356
    0.8500    0.2977
    0.9000    0.3753
    0.9500    0.4727
    1.0000    0.5960

if system_dependent('IsDebugMode')==1, dbquit; end
rh_colorModel gel�scht
doit
Loading data/HP_Polyester_RectoVerso_params.txt...
Loading data/HP_Polyester/../Reference IT874-1680.txt...
	Row: 15 NUMBER_OF_FIELDS: 5
	Row: 19 NUMBER_OF_SETS: 1680
Loading data/HP_Polyester/HP_Polyester_Reflection_i1Pro2.txt...
	Row: 7 NUMBER_OF_FIELDS: 42
	Row: 11 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Transmission_Absolute.spd.txt...
	Row: 17 NUMBER_OF_FIELDS: 38
	Row: 22 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Medium.txt...
	Row: 8 NUMBER_OF_FIELDS: 38
	Row: 13 NUMBER_OF_SETS: 30
	* Stripping Duplicates...
	Found ink saturation @ C=50
	Found ink saturation @ CMY=85
	Found ink saturation @ K=50
Solving for Paper Parameters  (ca. 20s-5min)... Start @11:22:54
		### Bei lami = 99999 Exit flag: 3Squared 2-norm large: 0.86144
Elapsed time is 84.347102 seconds.
Elapsed time is 0.623915 seconds.
DONE Calculating Ink layer transmissions.
Optimizing scatter parameters...
Optimizing csc(lam) 10/16
Elapsed time is 0.829434 seconds.
		DONE Optimizing scatter parameters.
The squared 2-norm is quite large: 0.033692
Elapsed time is 0.860244 seconds.
DONE Calculating Ink layer transmissions.
Elapsed time is 0.017348 seconds.
DONE Predicting solid colorant spectra.
data/HP_Polyester/HP_Polyester_Reflection_Absolute_Prediction.spd-28.txt
*** Calculating Delta E...
	Used Colors: 16/16. Eliminated 0 saturated colors.
data/HP_Polyester/HP_Polyester_Reflection_Absolute_Prediction_dE.txt
Loading data/HP_Polyester/../Reference IT874-1680.txt...
	Row: 15 NUMBER_OF_FIELDS: 5
	Row: 19 NUMBER_OF_SETS: 1680
Loading data/HP_Polyester/HP_Polyester_Reflection_i1Pro2.txt...
	Row: 7 NUMBER_OF_FIELDS: 42
	Row: 11 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Transmission_Absolute.spd.txt...
	Row: 17 NUMBER_OF_FIELDS: 38
	Row: 22 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Medium.txt...
	Row: 8 NUMBER_OF_FIELDS: 38
	Row: 13 NUMBER_OF_SETS: 30
	* Stripping Duplicates...
	Found ink saturation @ C=50
	Found ink saturation @ CMY=85
	Found ink saturation @ K=50
Solving for paper parameters (approx. 20s-5min)... Started: 13-Aug-2017 12:24:12
		### Bei lami = 99999 Exit flag: 3Squared 2-norm large: 0.86144
Elapsed time is 85.814457 seconds.
Elapsed time is 0.613453 seconds.
DONE Calculating Ink layer transmissions.
Optimizing scatter parameters...
Optimizing csc(lam) 10/16
Elapsed time is 0.834069 seconds.
		DONE Optimizing scatter parameters.
The squared 2-norm is quite large: 0.033692
Elapsed time is 0.855026 seconds.
DONE Calculating Ink layer transmissions.
Elapsed time is 0.017395 seconds.
DONE Predicting solid colorant spectra.
data/HP_Polyester/HP_Polyester_Reflection_Absolute_Prediction.spd-32.txt
*** Calculating Delta E...
	Used Colors: 16/16. Eliminated 0 saturated colors.
data/HP_Polyester/HP_Polyester_Reflection_Absolute_Prediction_dE.txt
Loading data/HP_Polyester/../Reference IT874-1680.txt...
	Row: 15 NUMBER_OF_FIELDS: 5
	Row: 19 NUMBER_OF_SETS: 1680
Loading data/HP_Polyester/HP_Polyester_Reflection_i1Pro2.txt...
	Row: 7 NUMBER_OF_FIELDS: 42
	Row: 11 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Transmission_Absolute.spd.txt...
	Row: 17 NUMBER_OF_FIELDS: 38
	Row: 22 NUMBER_OF_SETS: 1680
	* Stripping Duplicates...
	* Collecting data/Reference ISO12642-2 (ANSI IT8.7-4) visual.txt...
Loading data/HP_Polyester/HP_Polyester_Medium.txt...
	Row: 8 NUMBER_OF_FIELDS: 38
	Row: 13 NUMBER_OF_SETS: 30
	* Stripping Duplicates...
	Found ink saturation @ C=50
	Found ink saturation @ CMY=85
	Found ink saturation @ K=50
<a href="matlab: opentoline('Z:\InVo\Byshko\laufend\backlit\Recto-Verso\recto-verso\rh_RectoVerso.m',216,1)">216 </a>			cm.Hebert.r_10_f1 = @(tu,beta) ...
cm.Hebert.r_10

ans =

    0.5963

rh_colorModel gel�scht
cl = clock;
(
 (
  
{Error: Expression or statement is incorrect--possibly unbalanced (, {, or [.
} 
cl

cl =

   1.0e+03 *

    2.0170    0.0080    0.0130    0.0110    0.0580    0.0539

doc clock
date

ans =

    '13-Aug-2017'

time
{Undefined function or variable 'time'.
} 
datetime

ans = 

  <a href="matlab:helpPopup datetime" style="font-weight:bold">datetime</a>

   13-Aug-2017 12:03:49

char(datetime)

ans =

    '13-Aug-2017 12:03:58'

t.datetime
{Undefined variable "t" or class "t.datetime".
} 
t = datetime

t = 

  <a href="matlab:helpPopup datetime" style="font-weight:bold">datetime</a>

   13-Aug-2017 12:04:09

t.
 t.
   
{Error: Expression or statement is incomplete or incorrect.
} 
datetime('now')

ans = 

  <a href="matlab:helpPopup datetime" style="font-weight:bold">datetime</a>

   13-Aug-2017 12:04:19

now

ans =

   7.3692e+05

datetime

ans = 

  <a href="matlab:helpPopup datetime" style="font-weight:bold">datetime</a>

   13-Aug-2017 12:05:11

disp(['Solving for paper parameters (approx. 20s-5min)... Start @', char(datetime)])
Solving for paper parameters (approx. 20s-5min)... Start @13-Aug-2017 12:05:18
disp(['Solving for paper parameters (approx. 20s-5min)... Started: ', char(datetime)])
Solving for paper parameters (approx. 20s-5min)... Started: 13-Aug-2017 12:05:33
doc lsqnonlin
num2str(99999)

ans =

    '99999'

doc fprintf
close all
0.707^2

ans =

    0.4998

doit
Loading data/HP_Polyester_RectoVerso_params.txt...
