%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

classdef rh_RectoVerso < rh_colorModel
	
	properties (Constant)
		
	end
	
	properties %(Hidden)
	end
	% The following properties can be set only by class methods
	properties (SetAccess = private) %GetAccess = private
	end
	
	events
		%InsufficientFunds
	end
	
	methods %#############################################################
		% Constructor ********************************************
		function cm = rh_RectoVerso()
		end
		
		
		
		%********************************************************
		function res = Hebert_calibrate(cm, dbg)
			% Find the Hebert-specific measurements of the print medium (paper)
			res=0;
			
			if ~dbg.load
				cm.Hebert_medium_measurements();
				
				% 0. PAPER PARAMETERS...
				cm.Hebert_paper_params_general();
				
				% A. PAPER PARAMETERS
				cm.Hebert_paper_params(cm.parm.PARAMETERIZATION_INPUT_GEOMETRY_REFL,cm.parm.PARAMETERIZATION_INPUT_GEOMETRY_TRAN);
				
                cm.save_state();% F�r Debugging. Damit man nicht immer von vorne anfangen muss
			end
			cm.plot_paperparms(111);
			
			input_mode = cm.parm.PARAMETERIZATION_INPUT_MODE;
			if strcmp(input_mode, 'refl')
				input_geo = cm.parm.PARAMETERIZATION_INPUT_GEOMETRY_REFL;
			elseif strcmp(input_mode, 'tran')
				input_geo = cm.parm.PARAMETERIZATION_INPUT_GEOMETRY_TRAN;
			else
				Das_sollte_nie_passieren
			end
			
			if cm.parm.PARAMETERIZATION_SCATTER_MODEL == 12 || ...
				cm.parm.PARAMETERIZATION_SCATTER_MODEL == 13
				cm.Hebert_scatter_optimizer(input_geo,input_mode);
			end
			
			% B. INK LAYER NORMAL TRANSMITTANCES
            % from transmittance, for comparison later
            cm.Hebert.tu_frTr = cm.Hebert_ink_layer_normal_transmittances('diffuse','tran','Neugebauer16');
            
            % from reflectance
			cm.Hebert.tu = cm.Hebert_ink_layer_normal_transmittances(input_geo, input_mode, strtrim(cm.parm.PARAMETERIZATION_WHICH_SAMPLES));
		end
		
		
		%********************************************************
		% Reads all the measurements of the print medium, which are required for
		% applying the (modified) recto-verso model
		function Hebert_medium_measurements(cm)
			
			% TransR: Transmission Recto up, no backing
			% TransV: Transmission Verso up, no backing
			
			% ReflR:  Reflectance Recto up, no backing
			% ReflV:  Reflectance Verso up, no backing
			
			% ReflR_Bb: Reflectance Recto up, Backing black
			% ReflV_Bb: Reflectance Verso up, Backing black
			% ReflBackBlack: Reflectance of Black Backing only (no medium)
			
			% ReflR_Bw: Reflectance Recto up, Backing white
			% ReflV_Bw: Reflectance Verso up, Backing white
			% ReflBackWhite: Reflectance of White Backing only (no medium)
			
			
			% TransAIR: Transmission Air only (no medium, no backing) <-- not used
			% ReflAIR: Reflectance Air only (no medium, no backing) <-- not used
			
			% Find the Hebert-specific measurements of the print medium
			cm.mediumMeas.TransRecto = cm.find_spec_by_sampleName(cm.mediumData,'TransR');
			cm.mediumMeas.TransVerso = cm.find_spec_by_sampleName(cm.mediumData,'TransV');
			if cm.parm.DATA_ASSIMILATE_MEDIUM_MEASUREMENTS
				% The measurement from the chart is the reference...
				T_R_chart   = cm.get_colorant_spec('tran',cm.get_colorant_idx('W')) ;
				len = length(T_R_chart);
				%figure;		hold on;
				%plot(T_R_chart,'color',[1 0 0]);
				%plot(cm.mediumMeas.TransRecto,'color',[0 1 0]);
				ratio_lam = T_R_chart ./ cm.mediumMeas.TransRecto(1:len);
				cm.mediumMeas.TransRecto = cm.mediumMeas.TransRecto(1:len) .* ratio_lam;
				cm.mediumMeas.TransVerso = cm.mediumMeas.TransVerso(1:len) .* ratio_lam;
				%plot(cm.mediumMeas.TransRecto,'color',[0 0 1]);
				
			end
			cm.mediumMeas.ReflRecto  = cm.find_spec_by_sampleName(cm.mediumData,'ReflR');% no backing
			cm.mediumMeas.ReflVerso  = cm.find_spec_by_sampleName(cm.mediumData,'ReflV');% no backing
			
			cm.mediumMeas.ReflRecto_BackWhite  = cm.find_spec_by_sampleName(cm.mediumData,'ReflR_Bw');
			cm.mediumMeas.ReflVerso_BackWhite  = cm.find_spec_by_sampleName(cm.mediumData,'ReflV_Bw');
			cm.mediumMeas.ReflBackWhite  = cm.find_spec_by_sampleName(cm.mediumData,'ReflBackWhite');
			if cm.parm.DATA_ASSIMILATE_MEDIUM_MEASUREMENTS
				% The measurement from the chart is the reference...
				R_RBw_chart   = cm.get_colorant_spec('refl',cm.get_colorant_idx('W')) ;
				%plot(R_RBw_chart,'+-','color',[1 0 0]);
				%plot(cm.mediumMeas.ReflRecto_BackWhite,'+-','color',[0 1 0]);
				ratio_lam = R_RBw_chart ./ cm.mediumMeas.ReflRecto_BackWhite(1:len);
				cm.mediumMeas.ReflRecto_BackWhite = cm.mediumMeas.ReflRecto_BackWhite(1:len) .* ratio_lam;
				cm.mediumMeas.ReflVerso_BackWhite = cm.mediumMeas.ReflVerso_BackWhite(1:len) .* ratio_lam;
				cm.mediumMeas.ReflBackWhite = cm.mediumMeas.ReflBackWhite(1:len) .* ratio_lam;
				%plot(cm.mediumMeas.ReflRecto_BackWhite,'+-','color',[0 0 1]);
			end
			
			cm.mediumMeas.ReflRecto_BackBlack  = cm.find_spec_by_sampleName(cm.mediumData,'ReflR_Bb');
			cm.mediumMeas.ReflVerso_BackBlack  = cm.find_spec_by_sampleName(cm.mediumData,'ReflV_Bb');
			cm.mediumMeas.ReflBackBlack  = cm.find_spec_by_sampleName(cm.mediumData,'ReflBackBlack');
		end
		
		
		%********************************************************
		function Hebert_paper_params_general(cm)
			% Sections 1. thru 4. relate to those params, which depend only on np (refractive index)
			% Sections 5. and up relate to the function setup including solid ink
			% layers and light scatter correction
			
			%## 1. Refractive Index
			% If a refractive index other than 1.5 shall be used
			cm.Hebert.np	= cm.parm.PARAMETERIZATION_REFRACTIVE_INDEX; % In the following, the refr. idx. of air is assumed as 1.0

			% Critical angle of total reflection...
			if cm.Hebert.np >= 1.
				cm.Hebert.th_tot = asin(1/cm.Hebert.np);
			else
				disp('Refraction index too low!');
			end
			
			%## 2. Directional Fourier reflectance/transmittance coefficients
			% (see equations 8-11/2006 or 5-8/2009)

            cm.Hebert.T_01_45 = 1 - cm.Fresnel_reflect_01(pi/4); % T_01(45) = 1- R_01(45)
			cm.Hebert.T_10_0 = 1 - cm.Fresnel_reflect_01(0);	 % T_10(0) = T_01(0) = 1 - R_01(0)

            
			%## 3. Diffuse (= integrative) Fourier reflectance/transmittance coefficients
			% (see equations 15-19/2006 or 9-12/2009)
			
			% Hebert: r_01 = 0.092 f�r n = 1.5 (Hebert 2009 Appendix A; calculated via eq. (9/2009))
			%cm.Hebert.r_01 = 0.091777959341923; % ermittelt mit obj.r_01_integrate()/brute force bei 10^6 Samples;
			% 11 Stellen �ndern sich nicht gegen die halbe Samplezahl.
			%cm.Hebert.r_01 =0.0917779593423512; % ermittelt mit obj.r_01_integrate/Matlab integrate()
			cm.Hebert.r_01 = cm.r_01_integrate(); 
			
			cm.Hebert.t_01 = 1 - cm.Hebert.r_01;
            
			%r_10 = 1 - t_10 = 1 - t_01/np^2 = 1 - ( 1 - r_01 ) / np^2
			cm.Hebert.r_10 = 1 - ( 1 - cm.Hebert.r_01 ) / cm.Hebert.np^2;
			
			
			%## 4. Hebert model functions, paper only...
			cm.Hebert.Rm_f = @(rho1,rho2,tau,r1,r2)...% (Eq. 40/2006)
                                (rho1 - r2.*(rho1.*rho2 - tau.^2) ) ./ ...
                                ((1-r1.*rho1) .* (1-r2.*rho2) - r1.*r2 .* tau.^2 );
			
			cm.Hebert.Tm_f = @(rho1,rho2,tau,r1,r2)...% (Eq. 41/2006)
                                tau ./ ...
                                ((1-r1.*rho1).*(1-r2.*rho2) - r1.*r2 .* tau.^2 );
                            
            % Reflectance according to the William Clapper model
            cm.Hebert.R_WC_f = @(rho_B, r) ...
                                    rho_B ./ (1 - rho_B .* r);
			
			%## 5. Hebert approximation functions, solid colorant layer...
            % Hebert 2009, Eq. 16
			cm.Hebert.r10_t_f = @(t, beta) ...
                                    cm.Hebert.r_10 * ( exp(t.^beta) - 1 ) / ( exp(1) - 1 ); 

            % for given r_10 Medium-Air 
			cm.Hebert.beta = cm.Hebert_fit_the_beta();
			
            % Hebert 2009, Eq. 16, modified for arbitrary r10, i.e. for the
            % substrates with refraction index other than n = 1.5
			cm.Hebert.r10_n_t_f = @(r10, t) r10 .* ( exp(t.^cm.Hebert.beta) - 1 ) / ( exp(1) - 1 ); 
			
				
			
			%## 6. SCATTER CORRECTION (which is NOT part of Hebert's Recto-Verso-Model)
			switch cm.parm.PARAMETERIZATION_SCATTER_MODEL
				case 0 % no scatter correction
					cm.Hebert.sc_f = @(b,c,x)	x;	% no scatter correction
				case 1 % linear scatter correction
					cm.Hebert.sc_f = @(b,c,x)	x .* (1-c) + c; % 0 ? c ? ?0.5
				case 2 % quadratic scatter correction
					cm.Hebert.sc_f = @(b,c,x)	(1-c-b).*x.^2 + b.*x + c; % c ? [0..0.4]; b ? [0.9..1.1]
				case 12 % quadratic scatter correction
					cm.Hebert.sc_f = @(b,c,x)	(1-c-b).*x.^2 + b.*x + c; % c ? [0..0.4]; b ? [0.9..1.1]
				case 13
					cm.Hebert.sc_f = @(b,c,x)	(1-c-b).*x.^2 + b.*x + c; % c ? [0..0.4]; b ? [0.9..1.1]
				otherwise
					throw(MException('rh_colorModel:UnknownScatterModel', ...
                          ['Scatter model (',int2str(cm.parm.PARAMETERIZATION_SCATTER_MODEL),') unknown!']));
			end
			
			
			%## 7. Final paper parameters for 45�:0� Geometry (reflection)
            
            % Hebert 2006, Eq. 24
            mu_f = @(theta0, n) (1 - (sin(theta0)/n)^2) ^ (-1/2);
            
			theta_in = pi/4;
            theta_ex = 0;

            % R (Recto/printed up) with ink for 45�:0� geometry
			cm.Hebert.R_RI45_f = @(rho1,rho2,tau,r1,r2,csc,tu,bsc)		...
                cm.Hebert.sc_f(bsc,csc, ...
                    cm.Hebert.T_01_45 .* tu.^mu_f(theta_in, cm.Hebert.np) .* ... % T_in
                    cm.Hebert.T_10_0  .* tu.^mu_f(theta_ex, cm.Hebert.np) / cm.Hebert.np^2 .* ... % T_ex
                    cm.Hebert.Rm_f(rho1, rho2, tau, cm.Hebert.r10_n_t_f(r1, tu), r2) ...
            );
        
			cm.Hebert.R_RI45_WC_f = @(rho_B, r01_t, tu)		...
                    cm.Hebert.T_01_45 .* tu.^mu_f(theta_in, cm.Hebert.np) .* ... % T_in
                    cm.Hebert.T_10_0  .* tu.^mu_f(theta_ex, cm.Hebert.np) / cm.Hebert.np^2 .* ... % T_ex
                    cm.Hebert.R_WC_f(rho_B, cm.Hebert.r10_n_t_f(r01_t, tu));
			
			%## 8. Final paper parameters for d:0� geometry (tranmission)
            
            % T (Recto/printed up) with ink
			cm.Hebert.T_RId0_f = @(rho1,rho2,tau,r1,r2,tu)	...
                                    cm.Hebert.t_01 .* ...% T_in
                                    cm.Hebert.T_10_0  .* tu.^mu_f(theta_ex, cm.Hebert.np) / cm.Hebert.np^2 .* ... % T_ex
                                    cm.Hebert.Tm_f(rho1,rho2,tau,cm.Hebert.r10_n_t_f(r1,tu),r2);

        end
		
		
	
		%********************************************************
		% In eq. (16/2009), Hebert proposes an approximation function to replace the
		% integral of eq. (13/2009), which still depends on t. Hebert gives the
		% parameter beta only for refr. idx. np=1.5 (beta = 2.945
		% In this function, we determine the beta for any arbitrary np
		function beta = Hebert_fit_the_beta(cm)
			% eq. (13/2009)
			%{
			R_10 = @(theta)	cm.Fresnel_reflect_10(theta);
			fun  = @(theta,t)	R_10(theta) .* t.^(2./cos(theta)) .* sin(2*theta); % eq. (13/2009)

			y0	= integral(@(theta)fun(theta,0),0,pi/2);

			y0	= integral(@(theta)fun(theta,1),0,pi/2);
			%}
			
			%### 1. calculate the exact values at certain steps...
			t0	= [.1 .2 .3 .4 .5 .6 .7 .8 .9];
			len = length(t0);
			y0	= zeros(1,len); % preallocation
			for ii=1:len
				%y0(ii)	= integral(@(theta)fun(theta,t0(ii)),0,pi/2);
				y0(ii)	= cm.r_10_integrate(t0(ii));
			end
			
			%### 2. fit the approximation to the exact values...
			objF = @(beta)	cm.Hebert.r10_t_f(t0, beta) - y0;
			
            % starting guess
            v0 = 2.9; 
			
            % bounds
            lb = 0.5;
			ub = 50;
            
			options = optimset('TolFun',1e-8,'Display','off');
            
            % Invoke optimizer
			[v,resnorm,residual,exitflag,output] = lsqnonlin(objF,v0,lb,ub,options);
	
			beta = double(v);
		end
		
		
		%********************************************************
		% Determines the spectral paper parameters. 
        % This is the KEY FUNCTION of the model!
		% geoInR must be dir45
		% geoInT must be diffuse
		function Hebert_paper_params(cm, geoInR, geoInT)
			
			disp(['Solving for paper parameters (approx. 20s-5min)... Started: ', char(datetime)]);
			
			if ~strcmp(geoInT, 'diffuse') && ~strcmp(geoInR, 'dir45')
				throw(MException('rh_colorModel:ForbiddenGeometry', 'Wrong geometry parameters passed.'));
            end

			% Messdaten holen...
			T_R_lam = cm.mediumMeas.TransRecto;
			T_V_lam = cm.mediumMeas.TransVerso;
			R_R_lam = cm.mediumMeas.ReflRecto;
			R_V_lam = cm.mediumMeas.ReflVerso;
			R_RBw1_lam = cm.mediumMeas.ReflRecto_BackWhite;
			R_RBb_lam  = cm.mediumMeas.ReflRecto_BackBlack;
			R_VBw_lam = cm.mediumMeas.ReflVerso_BackWhite;
			R_VBb_lam  = cm.mediumMeas.ReflVerso_BackBlack;
			R_RBw2_lam = cm.get_colorant_spec('refl',cm.get_colorant_idx('W')) ;% Doppel von R_RBw1_lam
			T_R2_lam   = cm.get_colorant_spec('tran',cm.get_colorant_idx('W')) ;% Doppel von T_R_lam
			
            R_RI45_f = cm.Hebert.R_RI45_f;
            T_RId0_f = cm.Hebert.T_RId0_f;

            % Reflectance, recto up, unprinted
            R_R_f = @(rho1,rho2,tau,r1,r2,csc,bsc)	...
                cm.Hebert.R_RI45_f(rho1,rho2,tau,r1,r2,csc,1,bsc);
            
            % Reflectance, verso up, unprinted
            R_V_f = @(rho1,rho2,tau,r1,r2,csc,bsc)	...
                cm.Hebert.R_RI45_f(rho2,rho1,tau,r1,r2,csc,1,bsc);
			
            % T-prime (Recto up)
            T_R_f = @(rho1,rho2,tau,r1,r2)		...
                cm.Hebert.T_RId0_f(rho1,rho2,tau,r1,r2,1);
            
            % T-prime (Verso up)
            T_V_f = @(rho1,rho2,tau,r1,r2)		...
            cm.Hebert.T_RId0_f(rho2,rho1,tau,r1,r2,1);

            % Part 1: INTRINSIC PAPER PARAMETERS...

            % Achtung: 
            % Die Refl- und Trans-Spektren k�nnen verschiedene L�ngen haben
            L = cm.lambda.nsamp;

            T_R = T_R_lam(1:L);
            T_R2 = T_R2_lam(1:L); % from Tran. Chart
            T_V = T_V_lam(1:L);

            R_R = R_R_lam(1:L);
            R_V = R_V_lam(1:L);

            R_RBw1 = R_RBw1_lam(1:L); % from Media Measurements
            R_RBw2 = R_RBw2_lam(1:L); % from Refl. Chart

            R_RBb = R_RBb_lam(1:L);

            R_VBw = R_VBw_lam(1:L); % from Media Measurements
            R_VBb = R_VBb_lam(1:L); % from Media Measurements

            % Wir setzen das r1 auf den Wert f�r Medium-Luft.
            % Dieser Wert wird auch sp�ter bei Hebert_ink_layer_normal_transmittances() 
            % verwendet.

            % Da nicht klar ist, ob dies der realen Geometrie beim Aufsetzen des
            % Messger�ts entspricht, sind alternative Ans�tze vorbehalten. Bitte
            % beachten, dass auch in Hebert_ink_layer_normal_transmittances()
            % derselbe Wert verwendet wird.                
            cm.Hebert.r1a = cm.Hebert.r_10;

            r1a = cm.Hebert.r1a; % Abk�rzung f�r hier.

            % ratio_B2W dient der Stabilisierung der Optimierung. Die zu
            % optimierenden Werte f�r rBw und rBb werden damit in ein
            % "sinnvolles" Verh�ltnis gedr�ckt.
            ratio_B2W = cm.mediumMeas.ReflBackBlack(1:L) ./ cm.mediumMeas.ReflBackWhite(1:L);

            % weight for usage of ratio_B2W
            w_ratioWB = cm.parm.PARAMETERIZATION_FIT1_WEIGHT_BW;
            
            % weight for Neugebauer
            w_Neug = sqrt(3);

            T_R_allNeug = cm.colorants.Tran(1:end,1:L); 
            R_R_allNeug = cm.colorants.Refl(1:end,1:L); 

            if cm.parm.PARAMETERIZATION_SCATTER_MODEL == 0
                
                numNeug = 0;	% no color
                neugStart = 16; % just the last one: CMYK [1..16]
                
                T_RId0_allNeug = @(v) 0;
                R_RI45_allNeug = @(v) 0;

                scatter_constraints = @(v) v(6*L+1); % csc_weiss = 0
                
            elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 1
                numNeug = 1;	% just 1 color

                % Am 30.8.2016 von CMYK auf K ge�ndert, damit auch Teil-Charts
                % mit nur 1 statt 12 Seiten benutzt werden k�nnen.
                neugStart = 9; % CMYK [1..16]; 16 - CMYK; 9 - K; 

                % Die csc aller Neugebauer-Farben identisch machen
                for i=1:16 
                    
                    if i==2, continue; end
                    
                    if i==1
                        scatter_constraints = @(v) v(6*L+i) - v(6*L+2);
                    else
                        scatter_constraints = @(v) [scatter_constraints(v), v(6*L+i) - v(6*L+2)];
                    end
                end
            elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 2 % 16x csc, 16x bsc
                numNeug = 15; % 15 is max. number usable
                neugStart = 2; % start at Cyan [1..16]
                scatter_constraints = @(v) v(6*L+1); % csc_weiss = 0

            elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 12 || ...% 16x csc(lam), 16x bsc
                    cm.parm.PARAMETERIZATION_SCATTER_MODEL == 13
                numNeug = 15; % 15 is max. number usable
                neugStart = 2; % start at Cyan [1..16]
                scatter_constraints = @(v) v(6*L+1); % csc_weiss = 0
            else
                dasgehtnicht;
            end

            % Note: cm.Hebert.T_RId0_f = @(rho1,rho2,tau,r1,r2,tu)
            
            % Note: cm.Hebert.R_RI45_f = @(rho1,rho2,tau,r1,r2,csc,tu,bsc)
            
            for i=0:numNeug-1 % Wei� wird ausgelassen
                nn = neugStart + i; % nn = Neugebauer-Index
                w_tran = 1;
                w_refl = 1;

                Ttmp = @(v) (T_RId0_f(v(1:L), ...
                                      v(L+1:2*L), ...
                                      v(2*L+1:3*L), ...
                                      r1a,  ...
                                      v(3*L+1:4*L), ...
                                      v((7+i)*L+1:(8+i)*L)) ...
                             - T_R_allNeug(nn,1:L)) ...
                            .* w_tran;

                Rtmp = @(v) (R_RI45_f(v(1:L), ...
                                      v(L+1:2*L), ...
                                      v(2*L+1:3*L), ...
                                      r1a, ...
                                      v(4*L+1:5*L), ...
                                      v(6*L+2+i), ...
                                      v((7+i)*L+1:(8+i)*L), ...
                                      v(6*L+18+i)) ...
                             - R_R_allNeug(nn,1:L)) ...
                            .* w_refl;

                if i==0
                    T_RId0_allNeug = @(v) Ttmp(v);
                    R_RI45_allNeug = @(v) Rtmp(v);
                else
                    T_RId0_allNeug = @(v) [T_RId0_allNeug(v), Ttmp(v)];
                    R_RI45_allNeug = @(v) [R_RI45_allNeug(v), Rtmp(v)];
                end
            end

rb_off = 0;

objF = @(v) [...rho1	rho2		tau			  r1   r2			 csc	   bsc/tu		bsc
	(T_R_f(		v(1:L), v(L+1:2*L), v(2*L+1:3*L), r1a, v(3*L+1:4*L))				    - T_R),...
	(T_V_f(		v(1:L), v(L+1:2*L), v(2*L+1:3*L), r1a, v(3*L+1:4*L))				    - T_V)*rb_off,...
	(R_R_f(		v(1:L), v(L+1:2*L), v(2*L+1:3*L), r1a, v(3*L+1:4*L), 0,		   1 )		- R_R)*rb_off,...
	(R_V_f(		v(1:L), v(L+1:2*L), v(2*L+1:3*L), r1a, v(3*L+1:4*L), 0,		   1 )		- R_V)*rb_off,...
...	% Now the Medium on different backing
	(R_R_f(		v(1:L), v(L+1:2*L), v(2*L+1:3*L), r1a, v(4*L+1:5*L), 0,		   1 )		- R_RBw1), ...
	(R_V_f(		v(1:L), v(L+1:2*L), v(2*L+1:3*L), r1a, v(4*L+1:5*L), 0,		   1 )		- R_VBw)*rb_off, ...
	(R_R_f(		v(1:L),	v(L+1:2*L), v(2*L+1:3*L), r1a, v(5*L+1:6*L), 0,		   1 )		- R_RBb)*rb_off, ...
	(R_V_f(		v(1:L),	v(L+1:2*L), v(2*L+1:3*L), r1a, v(5*L+1:6*L), 0,		   1 )		- R_VBb)*rb_off, ...
... % Now the medium with ink of various colors
	T_RId0_allNeug(v) * w_Neug, ...
	R_RI45_allNeug(v) * w_Neug, ...
...	% Now various constraints...
... % rBb = 0.05 * rBw; ensure adequate ratio between white and black backing
	(v(5*L+1:6*L) - ratio_B2W .* v(4*L+1:5*L) ) .* w_ratioWB * rb_off,... 
	v(6*L+33:7*L),... Dummy-Parameter
	scatter_constraints(v),...
	(v(5*L+1:6*L-1) - v(5*L+2:6*L)) * 0.25 * rb_off...0.25 1.0  0.20 ... <-smoothing of rBb
...	(v(5*L+1:6*L) - v(5*L+21)) * .20 ... <-smoothing of rBb with reference to the center of lambda
...	(v(4*L+1:5*L-1) - v(4*L+2:5*L)) * .02 ... <-smoothing of rBw
...	(v(3*L+1:4*L-1) - v(3*L+2:4*L)) * .01 ... <-smoothing of r2t
	(v(2*L+1:3*L-1) - v(2*L+2:3*L)) * 0.25 ...  <-smoothing of tau
... (v(L+1:2*L-1) - v(L+2:2*L)) * 0.1 ...  <-smoothing of rho2
...	(v(6*L+1)*1000)^1.  ... <-- so k�nnen wir einen Parameter auf Null zwingen
	];

o = ones(1,L);	z = zeros(1,L);
%		1		2		3		4		5		6	 ||	7							 ||	8	ff
%		rho1	rho2	tau		r2t		rBw		rBb	 ||	csc			bsc			nix	 ||	t_xxxx
v0 = [	o*0.8	o*0.9	o*0.4	o*0.1	o*0.3	o*0.05	z(1:16)		o(1:16)		z(33:L)	];	    % starting guess
lb = [	z		z		z		z		z		z		z(1:16)		o(1:16)*.7	z(33:L)	];		% lower bounds
ub = [	o		o		o		o		o		o		o(1:16)*.03	o(1:16)*2.0	o(33:L)	];		% upper bounds bsc:1.5

%			8... f�r t_u
            v0 = [v0	repmat(o*0.5,[1,numNeug]) ];
            lb = [lb	repmat(z,[1,numNeug])]; % mit negativen Untergrenzen gibt's komplexe Zahlen...
            ub = [ub	repmat(o,[1,numNeug])];

            options = optimset('TolFun',1e-8,'Display','off');

            tic
            
            % Invoke optimizer
            [v,resnorm,residual,exitflag,output] = lsqnonlin(objF,v0,lb,ub,options);
            if resnorm > .02
                fprintf('		### Bei lami = %d, exit flag =  %f., squared 2-norm = %f', 99999, exitflag, resnorm);
            elseif exitflag ~= 1
                fprintf('		### Bei lami = %d, exit flag = %f.', 99999, exitflag);
            end
            
            toc
            
            cm.Hebert.rho1 = v(1:L);
            cm.Hebert.rho2 = v(L+1:2*L);
            cm.Hebert.tau = v(2*L+1:3*L);
            cm.Hebert.r2t = v(3*L+1:4*L);
            cm.Hebert.rBw = v(4*L+1:5*L);
            cm.Hebert.rBb = v(5*L+1:6*L);
            cm.Hebert.csc = v(6*L+1:6*L+16); % csc
            cm.Hebert.bsc = v(6*L+17:6*L+32); % bsc

            for i=1:numNeug
                cm.dbg.HebertParms(7+i,:) = v((6+i)*L+1:(7+i)*L); % t_x1
            end

            % The cm.dbg.* parameters serve for the convenient copy/paste from Matlab to Excel
            cm.dbg.HebertParms(1,:) = cm.Hebert.rho1(:);
            cm.dbg.HebertParms(2,:) = cm.Hebert.rho2(:);
            cm.dbg.HebertParms(3,:) = cm.Hebert.tau(:);
            cm.dbg.HebertParms(4,:) = cm.Hebert.r2t(:);
            cm.dbg.HebertParms(5,:) = cm.Hebert.rBw(:);
            cm.dbg.HebertParms(6,:) = cm.Hebert.rBb(:);
            cm.dbg.HebertParms(7,:) = v(6*L+1:7*L); % csc, bsc
		end
		
		
		
		%********************************************************
		% Recalculates spectral csc values per color, once the csc/bsc conststants
		% have been determined
		function y = Hebert_scatter_optimizer(cm,geoIn,from_mode)
			% *** bisher nur f�r Recto-Farben *** 
			disp(['Optimizing scatter parameters...']);
			 
			exc = MException('rh_colorModel:UnknownGeometry',['GEOMETRY (',geoIn,') unknown!']);
			exc2= MException('rh_colorModel:UnknownMode',['MODE (tran/refl) (',from_mode,') unknown!']);

			if strcmp(geoIn,'dir45') 
				if ~strcmp(from_mode,'refl'),	throw(exc2);	end
			elseif ~strcmp(geoIn,'diffuse'),	throw(exc);		
			end
		
			%if strcmp(whichCol,'Neugebauer16')
				idxlist = 1:length(cm.colorantsList);
				srcspec = cm.get_colorant_spec(from_mode,idxlist);
				%cm.Hebert.dev = cm.colorants.dev(idxlist,:);
			
			[len,samp] = size(srcspec);
			nlam = length(cm.Hebert.tau);
				
			tic
			
			rho1	= cm.Hebert.rho1;
			rho2	= cm.Hebert.rho2;
			tau		= cm.Hebert.tau;
			
			% 1a. Plot
			%fig = figure('Position',[100,1000,1120,840]); % Default-Gr��e: 560,420
			%labels = {'from Tran'};
			%hold on;
			lam=cm.lambda.start:cm.lambda.delta:cm.lambda.end;
					
			if 0 <= cm.parm.PARAMETERIZATION_INKSMOOTHNESS && cm.parm.PARAMETERIZATION_INKSMOOTHNESS <= 1
				smooth_strength = cm.parm.PARAMETERIZATION_INKSMOOTHNESS;
			else
				throw(MException('rh_colorModel:Unknown',['PARAMETERIZATION_INKSMOOTHNESS beyond allowed range!']) )
			end
					
			
			for coli=1:len
				if floor(coli/10)*10 == coli
					disp(['Optimizing csc(lam) ',num2str(coli),'/',num2str(len)]);
				end
				
				if strcmp(from_mode,'refl')
					%figure(fig);
					%hold off;
					
					% ###. from Refl
					srcspecR = cm.get_colorant_spec('refl',idxlist);
					rBw		= cm.Hebert.rBw;
					R_		= srcspecR(coli,1:nlam) ;
					dev_	= cm.colorants.dev(coli,:) ; % 
					
					% Jetzt die Scatter-param. berechnen...
					[csc,bsc] = cm.Hebert_sc_par(dev_);
					
					
					% ###. den tran
 					srcspecT = cm.get_colorant_spec('tran',idxlist);
 					r2t		= cm.Hebert.r2t;
 					T_		= srcspecT(coli,1:nlam) ;

% 					col = [.5 0 .5];
% 					plot (lam, T_,'-','color',col, 'LineWidth',2);
% 					grid on;
%  					axis([cm.lambda.start,cm.lambda.end,0,inf]);
%  					hold on;
					if coli==1
						tu_fit1 = ones(1,nlam);
					elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 12
						tu_fit1 = cm.dbg.HebertParms(6+coli,:); % take it from fit1
					elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 13
						tu_fit1 = cm.Hebert.tu_frTr(coli,:);	% take it from tran-tu
					else
						brichdasab
					end
% 					col = [0 0 1];
% 					plot (lam, tu_fit1,'+-','color',col, 'LineWidth',1);
% 					plot (lam, cm.Hebert.T_RId0_f(rho1,rho2,tau,cm.Hebert.r1a,r2t,tu_fit1),'*-','color',col, 'LineWidth',1);
			
					% c/b neu fitten...
					%ct = rh_color(cm.lambda.start, cm.lambda.end, cm.lambda.delta);
					%weights = sqrt(sum(ct.xyz_bar));
					%plot(lam,weights);
					objF = @(v) [...
						(cm.Hebert.R_RI45_f(rho1,rho2,tau,cm.Hebert.r1a,rBw,v ,tu_fit1,bsc) - R_)  ,... % b = 1-c ==> lineare Korr.
						(v(1:end-1) - v(2:end)) * .01 ... <-smoothing
						];
					%	  csc	bsc
					v0 = [zeros(1,nlam)];%		ones(1,nlam)]; % starting guess
					lb = [zeros(1,nlam)];%		0.7*ones(1,nlam)]; % lower bounds
					ub = [0.03*ones(1,nlam)];% 1.5*ones(1,nlam)];

                    options = optimset('TolFun',1e-10,'Display','off');
					[v,resnorm,residual,exitflag,output] = lsqnonlin(objF,v0,lb,ub,options);   % Invoke optimizer
					cnew = double(v(1:nlam));
					cm.Hebert.csc_lam(coli,1:nlam) = cnew;
					bnew = bsc;
					
				else
					throw(exc2);
				end
			
				
			end
			cm.Hebert.csc0 = cm.Hebert.csc;
			cm.Hebert.csc = cm.Hebert.csc_lam;
			clear ('cm.Hebert.csc');
			toc
			disp('		DONE Optimizing scatter parameters.');
		end
			
			
		%********************************************************
		function tu_list = Hebert_ink_layer_normal_transmittances(cm,geoIn,from_mode,whichCol)
			% Hebert 2009 sec. 4.B. Colorant Normal Transmittances
			% *** bisher nur f�r Recto-Farben ***
			 
			exc = MException('rh_colorModel:UnknownGeometry',['GEOMETRY (',geoIn,') unknown!']);
			exc2= MException('rh_colorModel:UnknownMode',['MODE (tran/refl) (',from_mode,') unknown!']);
			exc3= MException('rh_colorModel:UnknownColorSource',['whichCol (',whichCol,') unknown!']);

			if strcmp(geoIn,'dir45') 
				if ~strcmp(from_mode,'refl') && ~strcmp(from_mode,'refl_william_clapper') 
					throw(exc2)
				end
			elseif ~strcmp(geoIn,'diffuse') 
				throw(exc);
			end
			
			if strcmp(whichCol,'Neugebauer16')
				idxlist = 1:length(cm.colorantsList);
				srcspec = cm.get_colorant_spec(from_mode(1:4),idxlist);
				cm.Hebert.dev = cm.colorants.dev(idxlist,:);
			elseif strcmp(whichCol,'Chart')
				if strcmp(from_mode,'refl')
                    srcspec = cm.reflData.spec;
                    cm.Hebert.dev  = cm.reflData.dev;
                else
                    srcspec = cm.tranData.spec;
                    cm.Hebert.dev  = cm.tranData.dev;
				end
			elseif strncmp(whichCol,'Wedge',5)
				wedgeCol = whichCol(6:end);
				wedge = cm.find_wedge(from_mode,wedgeCol);
				srcspec = wedge.spec;
				cm.Hebert.dev  = wedge.dev;
				
                if strcmp(from_mode,'refl')	
                    cm.dbg.measRefl = wedge.spec;
                else
                    cm.dbg.measTrans = wedge.spec;
				end
            else
                throw(exc3);
			end
			
			len = size(srcspec, 1);

            nlam = length(cm.Hebert.tau);
				
            % initialization
			tu_list = zeros(len,nlam);
			
            tic
			
			rho1	= cm.Hebert.rho1;
			rho2	= cm.Hebert.rho2;
			tau		= cm.Hebert.tau;
			
			% 1a. Plot
%             figure('Position',[100,1000,1120,840]); % Default-Gr��e: 560,420
%             grid on;
%             axis([cm.lambda.start,cm.lambda.end,0,inf]);
%             labels = {'from Tran'};
%             hold on;
%             lam=cm.lambda.start:cm.lambda.delta:cm.lambda.end;
					
			
			for coli=1:len
				if floor(coli/20)*20 == coli
					fprintf('Calculating ink layer normal transmission %d of %d.\n', coli, len);
				end
				
				if strcmp(from_mode,'tran')
					r2t		= cm.Hebert.r2t;
					T_		= srcspec(coli,1:nlam) ;
					
					objF = @(tu)	cm.Hebert.T_RId0_f(rho1,rho2,tau,cm.Hebert.r1a,r2t,tu) - T_ ;

                elseif strncmp(from_mode,'refl', 4)
					rBw		= cm.Hebert.rBw;
					R_		= srcspec(coli,1:nlam) ;
					dev_	= cm.Hebert.dev(coli,:) ; % 
					
					% Jetzt die Scatter-param. berechnen...
                    if cm.parm.PARAMETERIZATION_SCATTER_MODEL == 0
                        csc = 0;
                        bsc = 1;
                    elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 1
                        bsc = 1;
                        csc = cm.Hebert.csc(1);
                    elseif cm.parm.PARAMETERIZATION_SCATTER_MODEL == 2 || ...
						   cm.parm.PARAMETERIZATION_SCATTER_MODEL == 12 || ...
						   cm.parm.PARAMETERIZATION_SCATTER_MODEL == 13
                        [csc,bsc] = cm.Hebert_sc_par(dev_);
					end
					
					if 0 <= cm.parm.PARAMETERIZATION_INKSMOOTHNESS && cm.parm.PARAMETERIZATION_INKSMOOTHNESS <= 1
						smooth_strength = cm.parm.PARAMETERIZATION_INKSMOOTHNESS;
					else
						throw(MException('rh_colorModel:Unknown',['PARAMETERIZATION_INKSMOOTHNESS beyond allowed range!']) )
                    end
                    
                    if strcmp(from_mode,'refl')
                        
                        objF = @(tu) [(cm.Hebert.R_RI45_f(rho1,rho2,tau,cm.Hebert.r1a,rBw,csc,tu,bsc) - R_), ...
                                      (tu(1:end-1) - tu(2:end)) * smooth_strength ...
                                     ];
                                 
                    elseif strcmp(from_mode, 'refl_william_clapper')
                        
                        rho_B = cm.mediumMeas.ReflRecto_BackWhite(1:cm.lambda.nsamp);
                        
                        objF = @(tu) [(cm.Hebert.R_RI45_WC_f(rho_B, cm.Hebert.r1a, tu) - R_), ...
                                      (tu(1:end-1) - tu(2:end)) * smooth_strength ...
                                     ];                    
                    end
				else
					throw(exc2);
				end
				v0 = ones(1,nlam)*0.5; % starting guess
				lb = zeros(1,nlam); % lower bounds
				ub = ones(1,nlam);
				
				options = optimset('TolFun',1e-10,'Display','off'); % 1e-8
				[v,resnorm,residual,exitflag,output] = lsqnonlin(objF,v0,lb,ub,options);   % Invoke optimizer
				if resnorm > .01
					disp(['The squared 2-norm is quite large: ',num2str(resnorm)]);
				elseif exitflag <= 0
					disp('### Da hat was nicht geklappt ###');
				end
				
				tu_list(coli,:) = double(v);
				
			end
			toc
			disp('DONE Calculating Ink layer transmissions.');
        end
        
        
        %********************************************************
		function T_lam = Hebert_predict_tran(cm,dev)
			
			t_01	= cm.Hebert.t_01;
			T_10	= cm.Hebert.T_10_0;
			np		= cm.Hebert.np;
			
			akrecto = cm.Hebert_Demichel(dev,muss_angepasst_werden); % Area coverages of the 2^N colorants
			akverso = [1,0,0,0,0,0,0,0];		% Only white, no colorants, on the verso
			
			syms tu
			%beta = 2.945; % param. of the approx. r_10(t)
			r_10	= cm.Hebert.r_10;
			
			ttk_recto_lam = cm.Hebert.tu;
			ttk_verso_lam = ttk_recto_lam; % we only use white, for which recto = verso

			T_in_lam	= t_01 *		akverso * ttk_recto_lam; % NO PRINTS ON THE VERSO SIDE!!!!! 
			T_ex_lam	= T_10 * np^-2 * akrecto * ttk_recto_lam;

			temp = cm.Hebert.r_10_f(ttk_recto_lam);
			r1_lam	= akrecto * double(temp); % Matrix product

			temp = cm.Hebert.r_10_f(ttk_verso_lam); % 
			r2_lam	= akverso * double(temp); % Matrix product
			

			tau_lam = cm.Hebert.tau;
			rho_lam = cm.Hebert.rho;
			
			zaehler = T_in_lam .* T_ex_lam .* tau_lam;
			nenner = (1 - rho_lam .* r1_lam) .* (1 - rho_lam .* r2_lam) - r1_lam .* r2_lam .* tau_lam.^2;
			
			T_lam	= zaehler ./ nenner;
			
		end
		
		
		%********************************************************
		function [csc,bsc] = Hebert_sc_par(cm,dev)
			% Calculates the respective scatter parameters for a given device color
			% set
			% cm.colorantsList = {'W','C','M','Y','CM','CY','MY','CMY','K','CK','MK','YK','CMK','CYK','MYK','CMYK'};
			
			ak = cm.Hebert_Demichel(dev);
			
			[num,~] = size(cm.Hebert.csc);
			if num==16 
				csc = ak * cm.Hebert.csc; % Matrix-prod (1x16).(16x33)
			else % num==1
				csc = sum(ak .* cm.Hebert.csc);
			end
			bsc = sum(ak .* cm.Hebert.bsc);
			
		end
					
		
		%********************************************************
		function Hebert_predict_solid_colors(cm,geoOut,to_mode,tuList)
			% Hebert 2009 sec. 4.B. Colorant Normal Transmittances
			% *** bisher nur f�r Recto-Farben ***
			
			exc = MException('rh_colorModel:UnknownGeometry',['GEOMETRY (',geoOut,') unknown!']);
			exc2= MException('rh_colorModel:UnknownMode',['MODE (tran/refl) (',to_mode,') unknown!']);

			if strcmp(geoOut,'dir45')
				if ~strcmp(to_mode,'refl'), throw(exc2), end
			elseif ~strcmp(geoOut,'diffuse')
				throw(exc)
			end
			
			rho1 = cm.Hebert.rho1;
			rho2 = cm.Hebert.rho2;
			tau  = cm.Hebert.tau;
            
			tic
			[len, nlam] = size(tuList);
			for coli=1:len
				if floor(coli/50)*50 == coli
					disp(['Predicting solid colorant spectrum ',num2str(coli),'/',num2str(len)]);
				end
				
				%tu = cm.Hebert.tu(coli,:);
				tu = tuList(coli,:);
					
				if strcmp(to_mode,'tran')
					r2t = cm.Hebert.r2t;
					pred = cm.Hebert.T_RId0_f(rho1,rho2,tau,cm.Hebert.r_10,r2t,tu);
					
				elseif strcmp(to_mode,'refl')
					rBw = cm.Hebert.rBw;

                    dev_ = cm.Hebert.dev(coli,:);
					
					% Jetzt die Scatter-param. berechnen...
					[csc,bsc] = cm.Hebert_sc_par(dev_);
					
					pred = cm.Hebert.R_RI45_f(rho1,rho2,tau,cm.Hebert.r_10,rBw,csc,tu,bsc);
					
				else
					throw(exc2);
				end
				cm.outData.spec(coli,:) = pred;
				
			end
			cm.outData.lambda = cm.lambda;
			toc
            
			disp('DONE Predicting solid colorant spectra.');
		end
		
		
		%********************************************************
		function aak = Hebert_Demichel(cm,dev)
			% device values CMYK are expected in the range 0..100
			
			if sum(size(cm.wedgeRefl))==0 || sum(size(cm.wedgeTran))==0
				throw(MException('rh_colorModel:Unknown', ['The wedges for TVI interpolation do not exist!']));
			end
			TVf = @(NeugChan,x) cm.TVrefl_f(NeugChan,x);
			
			c0 = dev(1)/100;
			m0 = dev(2)/100;
			y0 = dev(3)/100;
			k0 = dev(4)/100;
			
			c = c0;
			m = m0;
			y = y0;
			k = k0;
			%					1	2	3	4	5	 6	  7		8	 9	 10	  11   12	13	  14	15	  16
			%cm.colorantsList={'W','C','M','Y','CM','CY','MY','CMY','K','CK','MK','YK','CMK','CYK','MYK','CMYK'};
%{			
c = (1-m)*(1-y)*TVf(2,c) + m*(1-y)*TVf(5,c) + (1-m)*y*TVf(6,c) + m*y*TVf(8,c);
m = (1-c)*(1-y)*TVf(3,m) + c*(1-y)*TVf(5,m) + (1-c)*y*TVf(7,m) + c*y*TVf(8,m);
y = (1-c)*(1-m)*TVf(4,y) + c*(1-m)*TVf(6,y) + (1-c)*m*TVf(7,y) + c*m*TVf(8,y);
%}			
			last = [c m y k];
			done = false;
			while ~done
c = (1-m)*(1-y)*(1-k)*TVf(2,c0) + m*(1-y)*(1-k)*TVf(5,c0) + (1-m)*y*(1-k)*TVf(6,c0) +   m  * y *(1-k)*TVf(8,c0)  +...
    (1-m)*(1-y)*k	 *TVf(10,c0)+ m*(1-y)*k	  *TVf(13,c0)+ (1-m)*y*k	   *TVf(14,c0)+   m  * y *  k  *TVf(16,c0) ;
m = (1-c)*(1-y)*(1-k)*TVf(3,m0) + c*(1-y)*(1-k)*TVf(5,m0) + (1-c)*y*(1-k)*TVf(7,m0) +   c  * y *(1-k)*TVf(8,m0)  +...
    (1-c)*(1-y)*k	 *TVf(11,m0)+ c*(1-y)*k	  *TVf(13,m0)+ (1-c)*y*k	   *TVf(15,m0)+   c  * y *  k  *TVf(16,m0) ;
y = (1-c)*(1-m)*(1-k)*TVf(4,y0) + c*(1-m)*(1-k)*TVf(6,y0) + (1-c)*m*(1-k)*TVf(7,y0) +   c  * m *(1-k)*TVf(8,y0)  +...
    (1-c)*(1-m)*k	 *TVf(12,y0)+ c*(1-m)*k	  *TVf(14,y0)+ (1-c)*m*k	   *TVf(15,y0)+   c  * m *  k  *TVf(16,y0);
k = (1-c)*(1-m)*(1-y)*TVf(9,k0) + c*(1-m)*(1-y)*TVf(10,k0)+ (1-c)*m*(1-y)*TVf(11,k0)+ (1-c)*(1-m)*y  *TVf(12,k0) +...
      c  *  m  *(1-y)*TVf(13,k0)+ c*(1-m)*  y  *TVf(14,k0)+ (1-c)*m*  y  *TVf(15,k0)+   c  *  m  *y  *TVf(16,k0);
				done = max(abs([c m y k] - last)) < 1e-9;
				last = [c m y k];
			end

			
			% Demichel equations eq.(35) 2009
			a = [];
			a.w		= (1-c)*(1-m)*(1-y)*(1-k);
			a.c		=   c  *(1-m)*(1-y)*(1-k);
			a.m		= (1-c)*  m  *(1-y)*(1-k);
			a.y		= (1-c)*(1-m)*  y  *(1-k);
			a.cm	=   c  *  m  *(1-y)*(1-k);
			a.cy	=   c  *(1-m)*  y  *(1-k);
			a.my	= (1-c)*  m  *  y  *(1-k);
			a.cmy	=   c  *  m  *  y  *(1-k);
			
			a.k		= (1-c)*(1-m)*(1-y)*  k  ;
			a.ck	=   c  *(1-m)*(1-y)*  k  ;
			a.mk	= (1-c)*  m  *(1-y)*  k  ;
			a.yk	= (1-c)*(1-m)*  y  *  k  ;
			a.cmk	=   c  *  m  *(1-y)*  k  ;
			a.cyk	=   c  *(1-m)*  y  *  k  ;
			a.myk	= (1-c)*  m  *  y  *  k  ;
			a.cmyk	=   c  *  m  *  y  *  k  ;
			
			aak = [a.w,a.c,a.m,a.y,a.cm,a.cy,a.my,a.cmy, ...
				a.k,a.ck,a.mk,a.yk,a.cmk,a.cyk,a.myk,a.cmyk];
			tmp = sum(aak);
			if abs(tmp-1) > 1e-12
                disp(['tmp=',tmp,'  tmp-1=',tmp-1]);
                brichmich(); 
            end
			
		end
		
		
		%********************************************************
		function y = Fresnel_reflect_01(cm,th)
			% Air --> Paper; th = the incident polar angle theta
			y = cm.reflectance_unpol(th,cm.Hebert.np);
		end
		
		%********************************************************
		function y = Fresnel_reflect_10(cm,th)
			% Paper --> Air
			y = cm.reflectance_unpol(th,1/cm.Hebert.np);

			% Jetzt noch die komplexen Werte eliminieren...
			num = length(th);
			for ii = 1:1:num
				if th(ii) >= cm.Hebert.th_tot
					y(ii) = 1;
				end
			end
			
		end
		

		
		
		
		%********************************************************
		function y = r_01_integrate(obj) 
			% Reflects eq. (9) of the 2009 paper (r_01 for t=0 / paper only)
			% eq. (10/2009) can be replaced by eq. (9/2009) and the following:
			% r_10 = 1 - ( 1 - r_01 ) / np^2. 
			
			% Brute force integration... (takes approx. 1 minute)
			%{
			tic
			dTh = pi/2/1.e6;
			y = 0;
			for th = 0:dTh:pi/2
				y = y +  obj.Fresnel_reflect_01(th) * sin(2*th) * dTh;
			end
			% obj.r_01 = y
			toc
			%}
			
			% Integration using Matlab function (fast)...
			%tic
			R_01 = @(th)	obj.Fresnel_reflect_01(th);
			fun  = @(th)	R_01(th) .* sin(2*th); % eq. (9/2009)

			y	= integral(fun,0,pi/2);
			%toc
			
		end
			
		%********************************************************
		function y = r_10_integrate(obj,tt) 
			% Reflects eq. (13) of the 2009 paper, and eq. (10) for t = 1
			% It is also tabulated in Table 2 of the 2006 paper for np=1.5
			% The difference to r_01_integrate() is that t<>0 here. It is not clear
			% if a replacement for r_10 as in r_01_integrate() is possible here.
			
			% Brute force integration... (takes long)
			%{
			dTh = pi/2/1000;
			y = 0;
			for th = 0:dTh:pi/2-dTh % wegen der 1/cos-Fkt. bleiben wir vom Rand etwas weg,
				% sonst ergeben sich wegen numerischer Ungenauigkeit
				% "gerne" Unendlichkeitsstellen.
				R_10 = obj.Fresnel_reflect_10(th);
				zweidurchcos = 2/cos(th);
				dy = tt.^(zweidurchcos) * R_10 * sin(2*th) * dTh;
				y = y + dy;
			end
			%}
			
			% Integration using Matlab function (fast)...
			R_10 = @(theta)		obj.Fresnel_reflect_10(theta);
			fun  = @(theta,t)	R_10(theta) .* t.^(2./cos(theta)) .* sin(2*theta); % eq. (13/2009)

			y	= integral(@(theta)fun(theta,tt),0,pi/2);

		end
		
		
		
	end
	
	methods (Static = true) %#############################################################
		
		%********************************************************
		function y = reflectance_unpol(th,nn) 
			% Returns the reflectance for unpolarized light, assuming an equal mix of s-
			% and p-polarization
			% Arguments: th = the incident polar angle theta
			%			 nn = n2/n1 = the ratio of refractive indices of the incidence
			%			 medium 1 and the transmittance medium 2
			% Info source: any physics text book. See also https://en.wikipedia.org/wiki/Fresnel_equations
			
			y = ( reflectance_s(th,nn) + reflectance_p(th,nn) ) / 2;
			
			
			function y = reflectance_s(th,nn)
				%Returns the reflection for s-polarized light
				%Arguments: theta and nn=n2/n1
				
				%y = (n1*cos(th) - n2 * sqrt( 1-(n1/n2*sin(th))^2 ) )^2  /  (n1*cos(th) + n2 * sqrt( 1-(n1/n2*sin(th))^2 ) )^2;
				%y = ( cos(th) - n2/n1 * sqrt( 1-(n1/n2*sin(th))^2 ) )^2  /  ( cos(th) + n2/n1 * sqrt( 1-(n1/n2*sin(th))^2 ) )^2;
				y = ( cos(th) - nn * sqrt( 1-(1/nn*sin(th)).^2 ) ).^2  ./  ( cos(th) + nn * sqrt( 1-(1/nn*sin(th)).^2 ) ).^2;
				
			end
			
			function y = reflectance_p(th,nn)
				%Returns the reflection for p-polarized light
				%Arguments: theta and nn=n2/n1
				
				%y = (n1 * sqrt( 1-(n1/n2*sin(th))^2 ) - n2*cos(th) )^2  /  (n1 * sqrt( 1-(n1/n2*sin(th))^2 ) + n2*cos(th) )^2;
				y = ( sqrt( 1-(1/nn*sin(th)) .^2 ) - nn*cos(th) ).^2  ./  ( sqrt( 1-(1/nn*sin(th)).^2 ) + nn*cos(th) ).^2;
				
			end
		end
	
	end
	
end