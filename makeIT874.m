%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

function makeIT874(inFile)
	disp(123)
	
	dataF = 'data/';
	trgLambda = [];
	trgLambda.start = 0; % keine Validierung
  			
	fileData = rh_CGATS([dataF,inFile]);
	fileData.read_CGATS_file(trgLambda,true);
	
	res = fileData.write_IT874_visual();
			
end