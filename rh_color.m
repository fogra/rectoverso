%
% Copyright (c) 2015-2016 by Patrick G. Herzog -- R&D in Colour. All rights reserved.
%
% This file is part of RectoVerso.

% RectoVerso is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% RectoVerso is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with RectoVerso.  If not, see <http://www.gnu.org/licenses/>.
%

classdef rh_color < handle
	%UNTITLED Summary of this class goes here
	%   Detailed explanation goes here
	
	properties %#############################################################
		test1
		
		% Spectral color matching curves from 380nm to 780nm in 10n steps
		xyz_bar = [0.001368	0.004243	0.01431	0.04351	0.13438	0.2839	0.34828	0.3362	0.2908	0.19536	0.09564	0.03201	0.0049	0.0093	0.06327	0.1655	0.2904	0.4334499	0.5945	0.7621	0.9163	1.0263	1.0622	1.0026	0.8544499	0.6424	0.4479	0.2835	0.1649	0.0874	0.04677	0.0227	0.01135916	0.005790346	0.002899327	0.001439971	0.0006900786	0.0003323011	0.0001661505	0.00008307527	0.00004150994;
			0.000039	0.00012	0.000396	0.00121	0.004	0.0116	0.023	0.038	0.06	0.09098	0.13902	0.20802	0.323	0.503	0.71	0.862	0.954	0.9949501	0.995	0.952	0.87	0.757	0.631	0.503	0.381	0.265	0.175	0.107	0.061	0.032	0.017	0.00821	0.004102	0.002091	0.001047	0.00052	0.0002492	0.00012	0.00006	0.00003	0.00001499;
			0.006450001	0.02005001	0.06785001	0.2074	0.6456	1.3856	1.74706	1.77211	1.6692	1.28764	0.8129501	0.46518	0.272	0.1582	0.07824999	0.04216	0.0203	0.008749999	0.0039	0.0021	0.001650001	0.0011	0.0008	0.00034	0.00019	0.00004999999	0.00002	0	0	0	0	0	0	0	0	0	0	0	0	0	0];
		
		% Standard illuminant D50 from 380nm to 780nm in 10n steps
		D50 = [24.5	29.9	49.3	56.5	60.1	57.8	74.8	87.3	90.6	91.4	95.1	92	95.7	96.6	97.1	102.1	100.8	102.3	100	97.7	98.9	93.5	97.7	99.3	99	95.7	98.8	95.7	98.2	103	99.1	87.4	91.6	92.9	76.8	86.5	92.6	78.2	57.7	82.9	78.3];
		
		lambda	% Lambda range
		refWht	% reference white for Lab conversions
	end
	
	methods %#############################################################
		% Constructor ********************************************
		function ob = rh_color(lamStart, lamEnd, dLam)
			%ob.filename		 = filename;
			ob.test1 = 4711;
			
			% Determining the common lambda range...
			if lamStart < 380 || lamStart > 780 || lamEnd > 780 || dLam ~= 10 || lamEnd <= lamStart
				throw(MException('rh_color:LambdaRangeMismatch','Demanded Lambda range is out of allowed limits!'));
			end
			ob.lambda.start = lamStart;
			ob.lambda.end = lamEnd;
			ob.lambda.delta = dLam;
			ob.lambda.nsamp = (ob.lambda.end - ob.lambda.start) / ob.lambda.delta + 1;
			
			index1 = (ob.lambda.start - 380) / ob.lambda.delta + 1;
			index2 = (ob.lambda.end - 380) / ob.lambda.delta + 1;
			
			ob.xyz_bar = ob.xyz_bar(:,index1:index2);
			ob.D50	= ob.D50(:,index1:index2);
			
			
			ob.refWht.Ynorm = ob.D50 * ob.xyz_bar(2,:)';
			ob.refWht.XYZ = ob.D50 * ob.xyz_bar' / ob.refWht.Ynorm;
		end
		
		
		%********************************************************
		% spec2XYZ acccepts either single spectra or an array of spectra.
		% The spectral vectors must be exactly as long as given by the lambda range
		function out = spec2XYZ(ob,specin)
			out = specin * diag(ob.D50) * ob.xyz_bar' / ob.refWht.Ynorm;
		end
		
		
		%********************************************************
		% XYZ2Lab accepts only single XYZ triplets
		function Lab = XYZ2Lab(ob,XYZin)
			if XYZin(1)/ob.refWht.XYZ(1) > (216/24389)
				xx = (XYZin(1)/ob.refWht.XYZ(1))^(1/3);
			else
				xx = (841/108) * XYZin(1)/ob.refWht.XYZ(1) + 16/116;
			end
			if XYZin(2)/ob.refWht.XYZ(2) > (216/24389)
				yy = (XYZin(2)/ob.refWht.XYZ(2))^(1/3);
			else
				yy = (841/108) * XYZin(2)/ob.refWht.XYZ(2) + 16/116;
			end
			if XYZin(3)/ob.refWht.XYZ(3) > (216/24389)
				zz = (XYZin(3)/ob.refWht.XYZ(3))^(1/3);
			else
				zz = (841/108) * XYZin(3)/ob.refWht.XYZ(3) + 16/116;
			end
			
			
			if XYZin(2)/ob.refWht.XYZ(2) > (216/24389)
				Lab(1) = 116 * yy - 16;
			else
				Lab(1) = 116 * (841/108) * XYZin(2)/ob.refWht.XYZ(2);
			end
			Lab(2) = 500 * (xx - yy);
			Lab(3) = 200 * (yy - zz);
			
		end
		
		
		%********************************************************
		% In order to test the object, do the following steps:
		% 1. Construct an object with 380/780/10
		% 2. Invoke the test function
		function dE = rh_color_test(ob)
			spec = [1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00	1.00;
				0.1323818	0.1654772	0.3389256	0.5673096	0.7354580	0.8127522	0.8402242	0.8378136	0.8264858	0.8205480	0.8152242	0.8100606	0.8077988	0.8012200	0.7953704	0.7908522	0.7805758	0.7743776	0.7729192	0.7719416	0.7759710	0.7743500	0.7717336	0.7771436	0.7912850	0.7958172	0.8020688	0.8019874	0.7962982	0.7968270	0.7960680	0.7955088	0.7983438	0.8000408	0.7960672	0.7913810	0.7937724	0.7936976	0.7954844	0.7952950	0.8056154
				0.2070244	0.2587806	0.3699716	0.5984144	0.8125496	0.9321278	0.9746662	0.9653068	0.9341132	0.9213370	0.9112384	0.8998318	0.8933052	0.8824984	0.8759562	0.8704908	0.8579932	0.8500090	0.8480618	0.8466836	0.8511200	0.8493200	0.8468222	0.8520448	0.8683800	0.8746566	0.8818188	0.8816622	0.8742318	0.8745906	0.8744110	0.8725316	0.8736912	0.8732502	0.8688058	0.8645480	0.8682042	0.8657752	0.8570658	0.8216524	0.8076686
				0.0059840	0.0074800	0.0232800	0.1080800	0.1875400	0.2776600	0.3205800	0.3307400	0.3307000	0.3322000	0.3333200	0.3345000	0.3347800	0.3345200	0.3342000	0.3340600	0.3324200	0.3307600	0.3331800	0.3387000	0.3422200	0.3451800	0.3494200	0.3599800	0.3821000	0.3953000	0.4037200	0.4073000	0.4112200	0.4143400	0.4165400	0.4212800	0.4239600	0.4265600	0.4289600	0.4305000	0.4328200	0.4353000	0.4377400	0.4416400	0.4436800
				0.0093760	0.0117200	0.0320400	0.1368200	0.2337800	0.3457800	0.4001400	0.4187600	0.4272800	0.4318600	0.4361800	0.4391600	0.4389800	0.4376000	0.4384600	0.4406800	0.4396600	0.4373400	0.4409400	0.4484800	0.4530800	0.4572000	0.4634000	0.4773000	0.5059400	0.5241600	0.5357800	0.5404400	0.5445400	0.5476400	0.5511400	0.5579000	0.5597400	0.5603600	0.5631400	0.5667200	0.5726400	0.5758200	0.5702800	0.5427400	0.5209200
				0.1337208	0.1671508	0.3377800	0.5598882	0.7186614	0.7848924	0.8064938	0.8028376	0.7906638	0.7847948	0.7794830	0.7744786	0.7718562	0.7655368	0.7604058	0.7559792	0.7462424	0.7407822	0.7383940	0.7367686	0.7404368	0.7381400	0.7351216	0.7390154	0.7487552	0.7510800	0.7551670	0.7544536	0.7482616	0.7480142	0.7470406	0.7454526	0.7472092	0.7480636	0.7437966	0.7387344	0.7401002	0.7395128	0.7408390	0.7402136	0.7506990
				0.1318294	0.1647868	0.3371502	0.5721116	0.7534028	0.8561756	0.9016104	0.9074436	0.9016798	0.8975554	0.8936924	0.8887328	0.8859196	0.8783684	0.8714608	0.8651256	0.8524392	0.8449440	0.8429846	0.8436398	0.8495898	0.8484700	0.8468220	0.8566204	0.8815962	0.8925514	0.9028142	0.9050060	0.9007694	0.9025318	0.9032212	0.9043856	0.9092484	0.9123180	0.9087276	0.9039068	0.9075298	0.9081440	0.9113726	0.9118772	0.9245162
				0.03122	0.03903	0.06118	0.08545	0.08941	0.08211	0.06477	0.05186	0.04092	0.03236	0.02694	0.02070	0.01681	0.01290	0.01057	0.00902	0.00907	0.00993	0.00829	0.00747	0.02921	0.11182	0.25980	0.42660	0.51985	0.61948	0.67599	0.70861	0.71253	0.71748	0.71892	0.71909	0.72230	0.72504	0.72275	0.72010	0.72144	0.72084	0.72162	0.72114	0.73491
				0.01096	0.01370	0.03107	0.05192	0.07055	0.09124	0.12643	0.14223	0.14636	0.14600	0.14342	0.13375	0.12087	0.09188	0.06341	0.03771	0.01896	0.01158	0.00581	0.00415	0.00501	0.00501	0.00500	0.00583	0.00500	0.00581	0.00500	0.00582	0.00496	0.00497	0.00498	0.00498	0.00500	0.00585	0.00749	0.00911	0.01328	0.03152	0.05647	0.10610	0.16210
				0.00443	0.00554	0.00737	0.01027	0.01048	0.00988	0.01014	0.01022	0.01180	0.01657	0.03090	0.09235	0.21133	0.38607	0.44140	0.43859	0.42611	0.41774	0.39728	0.38088	0.40812	0.43561	0.44300	0.44243	0.44238	0.44509	0.45371	0.46991	0.47695	0.47804	0.47181	0.46695	0.46070	0.45607	0.47074	0.50572	0.56371	0.63042	0.66930	0.70788	0.74079
				0.00372	0.00465	0.00512	0.00635	0.00646	0.00637	0.00654	0.00638	0.00649	0.00621	0.00634	0.00637	0.00620	0.00615	0.00620	0.00625	0.00628	0.00620	0.00632	0.00622	0.00626	0.00636	0.00635	0.00635	0.00635	0.00633	0.00635	0.00645	0.00641	0.00663	0.00674	0.00674	0.00687	0.00700	0.00707	0.00704	0.00706	0.00695	0.00695	0.00705	0.00672];
			
			XYZ_targ = [0.9639119081	1.0000000000	0.8246236090;
				0.7838884007	0.7838884007	0.7838884007;
				0.8620448445	0.8620448445	0.8620448445;
				0.3433750961	0.3433750961	0.3433750961;
				0.4535124604	0.4535124604	0.4535124604;
				0.7476978474	0.7476978474	0.7476978474;
				0.8598442422	0.8598442422	0.8598442422];
			
			Lab_targ = [100.0000000000	0.0000000000	0.0000000000
				90.9570465236	0.3857627230	-2.4049078845
				94.3997124431	0.9270735276	-4.4588100267
				65.2295886331	2.9128928663	4.0755747963
				73.1225759347	2.9699720756	6.0027112077
				89.2850479681	0.0879444161	-2.7178963561
				94.3056906953	0.7942191969	-1.5529015355
				40.0624269938	71.0662683465	23.9734556999
				19.0682027171	-7.3265642910	-40.2665738439
				69.2590348941	-7.4611324738	79.9607494245
				5.6721689534	0.2193839868	-0.1561547516];
			
			XYZ = ob.spec2XYZ(spec);
			for ii=1:length(XYZ)
				Lab(ii,:) = ob.XYZ2Lab(XYZ(ii,:));
				dE(ii) = ob.DeltaE_ab(Lab(ii,:),Lab_targ(ii,:))
				%dE00(ii) = ob.DeltaE_00(Lab(ii,:),Lab_targ(ii,:), 1,1,1)
				if dE(ii) > 1e-10
					throw(MException('rh_color:TESTROUTINE_ERROR',['dE Error in test routine! Target Lab=',...
						num2str(Lab_targ(ii,:)),'; Actual Lab=',num2str(Lab_targ(ii,:)),'; dE=',num2str(dE(ii))]));
				end
			end
			dE = ob.DeltaE_ab(Lab,Lab_targ)
				
			disp(['Test of rh_color object successful!']);
			
		end
		
		
	end % methods
	
	
	%#############################################################
	methods (Static = true) 
		
		%********************************************************
		% INput: cmyk [0..100]
		% output: rgb [0..1]
		% Accepts either a single cmyk quadruplet or a list of such
		function rgb = cmyk2rgb_simplex(cmyk)
			wk = 0.8;
			w1 = 0.75;
			w2 = 0.85;
			rgb(:,1) = ( (1 - w1 * cmyk(:,1)/100 ) .* (1 - wk * cmyk(:,4)/100 ) - (1-w1)*(1-wk) ) / (1 - (1-w1)*(1-wk));
			rgb(:,2) = ( (1 - w2 * cmyk(:,2)/100 ) .* (1 - wk * cmyk(:,4)/100 ) - (1-w2)*(1-wk) ) / (1 - (1-w2)*(1-wk));
			rgb(:,3) = ( (1 - w2 * cmyk(:,3)/100 ) .* (1 - wk * cmyk(:,4)/100 ) - (1-w2)*(1-wk) ) / (1 - (1-w2)*(1-wk));
			
		end
		
		
		%********************************************************
		% DeltaE_ab accepts either single pairs of Lab-triplets, or two arrays of
		% Lab-triplets
		function dE = DeltaE_ab(Lab1,Lab2)
			
			dE = sqrt((Lab2(:,1)-Lab1(:,1)).^2 + (Lab2(:,2)-Lab1(:,2)).^2 + (Lab2(:,3)-Lab1(:,3)).^2);
		end

		
		%**********************************************************************************************
		% DeltaE_00 requires 3 parameters (kL, kC, kH), downsizing the contributions of
		% dL, dC, and dH, resp. In case of doubt, choose 1/1/1
		%
		% HINWEIS: Gaurav Sharma zwingt alle Winkel zun�chst auf [0..2pi] w�hrend sie
		% hier in [-pi,pi] liegen. Dadurch ergibt sich ein kleiner Unterschied bei
		% Sharmas 14. Testfarbpaar. Die beiden Farben liegen sich in der ab-Ebene
		% �ber den Ursprung gegen�ber, haben also genau 180� Winkelverschiebung. Dies
		% resultiert darin, dass bei Sharma ein Durchschn. Winkel von etwas �ber pi
		% ermittelt wird, w�hrend dieser hier bei etwas �ber 0 liegt. Logisch gesehen ist
		% dies dasselbe. Jedoch ergeben sich f�r den Parameter T und damit f�r SH ein
		% etwas anderer Zahlenwert, so dass der ermittelte dE-Wert sich um 0,0584
		% unterscheidet. Das ist in der Praxis irrelevant.
		% In diesen F�llen, bei denen sich die beiden Lab-Farben
		% diametral gegen�ber liegen, macht das Konzept der bunttonabh�ngigen
		% Korrektur keinen Sinn. Bei kleinen Chroma wirkt es sich wenig aus (wie
		% hier), w�hrend es bei gro�en Chromas zu unsinnigen Werten f�hrt. Alternativ
		% k�nnte man statt des mittleren Bunttons den der Referenzfarbe nehmen, dann
		% gibt es aber Unsymmetrien, je nachdem welche Farbe die Referenz ist.
		function dE = DeltaE_00 ( LabReferenz, Lab2, kL, kC, kH) % kL = {1,2}
			
			Degree = pi/180;
			
			Lmean = (Lab2(:,1) + LabReferenz(:,1)) / 2;% mittelwert fuer L
			
			% Berechnung von a-,C-,h-, deltah-Prime (Strich)
			C1 = sqrt(LabReferenz(:,2).*LabReferenz(:,2) + LabReferenz(:,3).*LabReferenz(:,3));
			C2 = sqrt( Lab2(:,2).*Lab2(:,2) + Lab2(:,3).*Lab2(:,3) );
			Cmean = (C1+C2)/2;   % Mittelwerte berechnen
			Cfact = Cmean.^7;   % fuer G benoetigt.
			
			G = 0.5*(1 - sqrt(Cfact./(Cfact + 25^7)));
			
			aP1 = (1 + G).*LabReferenz(:,2)	;% a prime (L prime = L und b prime = b)
			aP2 = (1 + G).*Lab2(:,2);
			
			CP1 = sqrt(aP1.*aP1 + LabReferenz(:,3).*LabReferenz(:,3)); % CP = C prime
			CP2 = sqrt(aP2.*aP2 + Lab2(:,3).*Lab2(:,3));
			CPmean = (CP1+CP2)/2;
			
			hP1 = atan2(LabReferenz(:,3),aP1)	;% h prime
			hP2 = atan2(Lab2(:,3),aP2);
			dh = hP2 - hP1;
			%{
			if (dh > pi)
				dh = dh - 2.*pi;
				hP2 = hP2 - 2.*pi;    %vom groesseren Winkel muessen 360 Grad abgezogen werden!
			elseif (dh < -pi)
				dh = dh + 2.*pi;
				hP1 = hP1 - 2.*pi;
			end
			%}
			% Arrayf�hige Variante:
			hP2 = hP2 - 2*pi*(dh > pi); % Wenn die dh > pi, muss vom gr��eren Winkel 2pi abgezogen werden.
			dh	= dh - 2*pi*(dh > pi);  % dh wird danach korrigiert
			hP1 = hP1 - 2*pi*(dh < -pi);
			dh	= dh + 2*pi*(dh < -pi);

			
			hPmean = (hP1+hP2)/2;
			%if (hPmean < 0)
			%	hPmean = hPmean + 2*pi;		end
			hPmean = hPmean + 2*pi*(hPmean < 0);
			
			% Berechnung von DeltaL-, DeltaC-, DeltaH-prime
			myLCHE.dL = (Lab2(:,1)-LabReferenz(:,1));
			myLCHE.dC = CP2 - CP1;
			myLCHE.dH = 2*sqrt(CP2.*CP1).*sin(dh/2);
			
			% Berechnung der S- und T-Faktoren
			T = 1 - 0.17*cos(hPmean-30*Degree) + 0.24*cos(2*hPmean)...
				+ 0.32*cos(3*hPmean+6*Degree) - 0.20*cos(4*hPmean-63*Degree);
			
			SL = 1 + (0.015*(Lmean-50).*(Lmean-50)) ./ (sqrt(20 + (Lmean-50).*(Lmean-50)));
			SC = 1 + 0.045*CPmean;
			SH = 1 + 0.015*CPmean.*T;
			
			% Berechnung von RC, deltaTheta und RT
			RC = 2*sqrt(CPmean.^7 ./ (CPmean.^7 + 25^7));
			temp = ((hPmean/Degree - 275)/25).^2;
			
			deltaTheta = 30*exp(-temp);
			RT = -sin(2*deltaTheta*Degree).*RC;
			
			% Berechnung von DeltaE00
			myLCHE.dL = myLCHE.dL ./ (kL*SL);
			myLCHE.dC = myLCHE.dC ./ (kC*SC);
			myLCHE.dH = myLCHE.dH ./ (kH*SH);
			
			myLCHE.dE = sqrt( myLCHE.dL.*myLCHE.dL + myLCHE.dC.*myLCHE.dC + myLCHE.dH.*myLCHE.dH ...
				+ RT.*myLCHE.dC.*myLCHE.dH );
			dE = myLCHE.dE;
			return 
		end
		
		
		
		%**********************************************************************************************
		% Testing the DeltaE_00 formula. Based on a test script from Gaurav Sharma (http://www.ece.rochester.edu/~gsharma/ciede2000/)
		% Tests the result of DeltaE_00 against the test data in the file ciede2000testdata.txt
		function res = TEST_DeltaE_00()
			% Load test data
			load ciede2000testdata.txt
			% Each row of the file has a pair of CIELAB values and a color difference.
			% For each row, columns 1-3 of the file correspond to CIE L*, a*, b* values,
			% respectively for a reference color; columns 4-6 correspond to L*, a*, b*
			% values, respectively for a sample color; and column 7 corresponds to
			% the CIEDE2000 color difference between this pair of values.
			
			Labstd = ciede2000testdata(:,1:3);
			Labsamp = ciede2000testdata(:,4:6);
			dE00result = ciede2000testdata(:,7);
%  			Labstd = ciede2000testdata(14:15,1:3)
% 			Labsamp = ciede2000testdata(14:15,4:6)
% 			dE00result = ciede2000testdata(14:15,7)
			
			% Compute the CIEDE2000 color difference between the pairs
			%dE00 = deltaE2000(Labstd,Labsamp);
 			%dE00 = dE00';
			dE00 = rh_color.DeltaE_00(Labstd,Labsamp,1,1,1); % pgh
			
			% Compare the computation vs the published result
			Nsamp = size(Labstd,1);
			vv= [1:Nsamp];
			plot(vv,dE00, '+', vv,dE00result, 'o');
			legend('Implemention','Published');
			
			% Repeat test by interchaging sample vs testdata order and
			% see whether differences remain same
			%dE00t = deltaE2000(Labsamp,Labstd);
			dE00t = rh_color.DeltaE_00(Labsamp,Labstd,1,1,1);
			vv= [1:Nsamp];
			figure; plot(vv,dE00t, '+', vv,dE00result, 'o');
			legend('Implement (sampNstdexch)','Published');
			
			% Numerically verify that the difference is small
			dif = dE00-dE00result
			maxi = max(abs(dif))
			% typical values around 4 x 10^-5
			if maxi> 6e-2
				throw(MException('rh_color:TESTFUNC_ERROR','DeltaE is not within the expected accuracy!'));
			end
		end

		
	end % static methods
end

